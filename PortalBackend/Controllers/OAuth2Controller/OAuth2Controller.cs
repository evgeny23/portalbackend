﻿using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Models;
using PortalBackend.Responses;
using PortalBackend.Security;
using PortalBackend.Services;
using System;
using System.Linq;
using System.Collections.Generic;

namespace PortalBackend.Controllers.OAuth2Controller
{
    [Route("oauth")]
    [ApiController]
    public class OAuth2Controller : ControllerBase
    {
        private int redisDb = (int)RedisDatabases.OAuth;

        /// <summary>
        /// Аутентификация по паре логин/пароль или email/пароль
        /// </summary>
        /// <response code="200">Аутентификация прошла успешно</response>
        /// <response code="401">Неверные логин или пароль</response>
        [HttpPost("authentication")]
        public IActionResult Authentication([FromBody] AuthenticationData data)
        {
            return OAuthDbMethods.Authorize(data).Resp();
        }

        /// <summary>
        /// Обновление пары токенов
        /// </summary>
        /// <response code="200">Обновление токенов успешно</response>
        /// <response code="400">Данный рефреш токен недействителен, ошибка</response>
        [HttpPost("refresh")]
        public IActionResult Refresh([FromBody] RefreshData data)
        {
            return OAuthDbMethods.Refresh(data).Resp();
        }

        /// <summary>
        /// Смена активного связанного аккаунта
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">Идентификатор текущего активного пользователя</param>
        /// <param name="relatedUserId">Идентификатор связанного аккаунта</param>
        /// <returns>Пара токенов</returns>
        [HttpGet("related/change/{userId}/{relatedUserId}")]
        [CheckPermissions(PermissionsEnum.Common, "userId", true)]
        public IActionResult ChangeRelatedAccount(int userId, int relatedUserId)
        {
            return OAuthDbMethods.ChangeRelatedAccount(userId, relatedUserId).Resp();
        }

        /// <summary>
        /// Тестовый роут для проверки работы access токена
        /// [CheckPermissions(PermissionsEnum.Common)]
        /// </summary>
        /// <response code="200">Работает</response>
        /// <response code="Любые другие">Не работает</response>
        [CheckPermissions(PermissionsEnum.Common)]
        [HttpGet("authentication/check_access")]
        public IActionResult CheckAccess()
        {
            return StandartResponses.Ok("Проверка пройдена").Resp();
        }

        ///// <summary>
        ///// Активация кода проверки email/sms
        ///// </summary>
        ///// <response code="200">Активация успешна</response>
        ///// <response code="Любые другие">Не валидный ключ/ошибка в параметрах</response>
        //[HttpGet("authentication/activation_code/{type}/{hash}/{code}")]
        //public IActionResult CheckActivationCode(string type, string hash, string code)
        //{
        //    ActivationCodes.Check(hash, code);
        //    return new BaseResponse()
        //    {
        //        message = "Проверка пройдена",
        //        ResponseType = ResponseType.Data,
        //        statusCode = 200,
        //    }.Resp();
        //}

        /// <summary>
        /// Logout
        /// </summary>
        /// <param name="token">refresh-токен</param>
        /// <response code="200">Рефреш-токен удалён</response>
        /// <response code="400">Рефреш-токен недействителен</response>
        [HttpPost("logout")]
        public IActionResult Logout([FromBody] RefreshData data)
        {
            if (data.token.Length > 10)
            {
                if (Redis.RemoveData(data.token, redisDb))
                {
                    return StandartResponses.Ok("Успешный logout").Resp();
                }
            }
            return StandartResponses.BadRequest("Токен недействителен").Resp();
        }
    }
}