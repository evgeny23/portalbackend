﻿using Microsoft.AspNetCore.Mvc;
using PortalBackend.Security;

namespace PortalBackend.Filters.CheckPermissions
{
    // Пример использования IAuthorizationFilter был взят тут: 
    // https://stackoverflow.com/questions/31464359/how-do-you-create-a-custom-authorizeattribute-in-asp-net-core

    public class CheckPermissions : TypeFilterAttribute
    {
        public CheckPermissions(PermissionsEnum permission, string userId = "", bool ignoreEmailValidate = false) : base(typeof(CheckPermissionsFilter))
        {
            Arguments = new object[] { permission, userId, ignoreEmailValidate };
        }
    }
}
