﻿using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Security;

namespace PortalBackend.Controllers.EducationController
{
    [Route("education_programms")]
    [ApiController]
    public class EducationController : ControllerBase
    {
        /// <summary>
        /// Добавление новой учебной программы
        /// </summary>
        /// <response code="200">Программа добавлена</response>
        /// <response code="400">Ошибка при создании</response>
        [HttpPost("new_programm")]
        [CheckPermissions(PermissionsEnum.Common)]
        public IActionResult CreateNewEducationProgramm([FromBody] EducationProgrammData data)
        {
            return EducationDbMethods.CreateNewEducationProgramm(data).Resp();
        }
    }
}
