﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using PortalBackend;

namespace PortalBackend.Migrations
{
    [DbContext(typeof(ApplicationContext))]
    [Migration("20210218123854_fix_edu")]
    partial class fix_edu
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasPostgresEnum(null, "permissions_enum", new[] { "admin", "common", "news", "calendar", "products" })
                .UseIdentityByDefaultColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.1");

            modelBuilder.Entity("PortalBackend.Models.BaseUser", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<bool>("DefaultAccount")
                        .HasColumnType("boolean");

                    b.Property<string>("Discriminator")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("ParentUserId")
                        .HasColumnType("integer");

                    b.Property<DateTime>("RegistrationDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("UserId");

                    b.ToTable("users");

                    b.HasDiscriminator<string>("Discriminator").HasValue("BaseUser");
                });

            modelBuilder.Entity("PortalBackend.Models.ChildrenParent", b =>
                {
                    b.Property<int>("ChildrenId")
                        .HasColumnType("integer");

                    b.Property<int>("ParentId")
                        .HasColumnType("integer");

                    b.HasKey("ChildrenId", "ParentId");

                    b.HasIndex("ParentId");

                    b.ToTable("children_parents");
                });

            modelBuilder.Entity("PortalBackend.Models.EduCourse", b =>
                {
                    b.Property<int>("EduId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("Desc")
                        .IsRequired()
                        .HasMaxLength(8096)
                        .HasColumnType("character varying(8096)");

                    b.Property<string>("Image")
                        .IsRequired()
                        .HasMaxLength(256)
                        .HasColumnType("character varying(256)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(512)
                        .HasColumnType("character varying(512)");

                    b.Property<string>("PageSource")
                        .IsRequired()
                        .HasColumnType("citext");

                    b.HasKey("EduId");

                    b.ToTable("edu_courses");
                });

            modelBuilder.Entity("PortalBackend.Models.Game", b =>
                {
                    b.Property<int>("GameId")
                        .HasColumnType("integer");

                    b.Property<string>("ProductId")
                        .HasColumnType("citext");

                    b.Property<string>("GameName")
                        .HasMaxLength(1024)
                        .HasColumnType("citext");

                    b.HasKey("GameId", "ProductId");

                    b.HasIndex("ProductId");

                    b.ToTable("product_games");
                });

            modelBuilder.Entity("PortalBackend.Models.GameTag", b =>
                {
                    b.Property<int>("GameId")
                        .HasColumnType("integer");

                    b.Property<string>("ProductId")
                        .HasColumnType("citext");

                    b.Property<string>("Tag")
                        .HasColumnType("citext");

                    b.HasKey("GameId", "ProductId", "Tag");

                    b.HasIndex("ProductId");

                    b.ToTable("games_tags");
                });

            modelBuilder.Entity("PortalBackend.Models.GartenPedagog", b =>
                {
                    b.Property<int>("GartenUserId")
                        .HasColumnType("integer");

                    b.Property<int>("PedagogUserId")
                        .HasColumnType("integer");

                    b.Property<int?>("KindergartenUserId")
                        .HasColumnType("integer");

                    b.HasKey("GartenUserId", "PedagogUserId");

                    b.HasIndex("KindergartenUserId");

                    b.HasIndex("PedagogUserId");

                    b.ToTable("gartens_pedagogs");
                });

            modelBuilder.Entity("PortalBackend.Models.Product", b =>
                {
                    b.Property<string>("ProductId")
                        .HasMaxLength(32)
                        .HasColumnType("citext");

                    b.Property<string>("ProductName")
                        .HasMaxLength(256)
                        .HasColumnType("citext");

                    b.HasKey("ProductId");

                    b.ToTable("products");
                });

            modelBuilder.Entity("PortalBackend.Models.ProductEducation", b =>
                {
                    b.Property<int>("EduId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("EduDesc")
                        .IsRequired()
                        .HasMaxLength(8096)
                        .HasColumnType("character varying(8096)");

                    b.Property<string>("EduName")
                        .IsRequired()
                        .HasMaxLength(1024)
                        .HasColumnType("character varying(1024)");

                    b.Property<string>("ImageLink")
                        .IsRequired()
                        .HasMaxLength(256)
                        .HasColumnType("character varying(256)");

                    b.Property<string>("PageSource")
                        .IsRequired()
                        .HasColumnType("citext");

                    b.Property<string>("ProductId")
                        .IsRequired()
                        .HasColumnType("citext");

                    b.HasKey("EduId");

                    b.HasIndex("ProductId");

                    b.ToTable("product_education");
                });

            modelBuilder.Entity("PortalBackend.Models.Region", b =>
                {
                    b.Property<string>("Country")
                        .HasColumnType("text");

                    b.Property<string>("State")
                        .HasColumnType("text");

                    b.Property<string>("City")
                        .HasColumnType("text");

                    b.HasKey("Country", "State", "City");

                    b.ToTable("regions");
                });

            modelBuilder.Entity("PortalBackend.Models.Role", b =>
                {
                    b.Property<int>("RoleId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<bool>("Custom")
                        .HasColumnType("boolean");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.HasKey("RoleId");

                    b.ToTable("roles");

                    b.HasData(
                        new
                        {
                            RoleId = 1,
                            Custom = false,
                            Name = "Администратор"
                        },
                        new
                        {
                            RoleId = 2,
                            Custom = false,
                            Name = "Зарегистрированный пользователь"
                        },
                        new
                        {
                            RoleId = 3,
                            Custom = false,
                            Name = "Детский сад"
                        },
                        new
                        {
                            RoleId = 4,
                            Custom = false,
                            Name = "Родитель"
                        },
                        new
                        {
                            RoleId = 5,
                            Custom = false,
                            Name = "Педагог"
                        });
                });

            modelBuilder.Entity("PortalBackend.Models.RolePermission", b =>
                {
                    b.Property<int>("Permission")
                        .HasColumnType("integer");

                    b.Property<int>("RoleId")
                        .HasColumnType("integer");

                    b.HasKey("Permission", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("role_permissions");

                    b.HasData(
                        new
                        {
                            Permission = 0,
                            RoleId = 1
                        },
                        new
                        {
                            Permission = 1,
                            RoleId = 2
                        },
                        new
                        {
                            Permission = 1,
                            RoleId = 4
                        },
                        new
                        {
                            Permission = 2,
                            RoleId = 4
                        },
                        new
                        {
                            Permission = 1,
                            RoleId = 3
                        },
                        new
                        {
                            Permission = 2,
                            RoleId = 3
                        },
                        new
                        {
                            Permission = 4,
                            RoleId = 3
                        },
                        new
                        {
                            Permission = 1,
                            RoleId = 5
                        },
                        new
                        {
                            Permission = 2,
                            RoleId = 5
                        });
                });

            modelBuilder.Entity("PortalBackend.Models.UserProduct", b =>
                {
                    b.Property<string>("SerialHash")
                        .HasColumnType("text");

                    b.Property<int>("UserId")
                        .HasColumnType("integer");

                    b.Property<string>("ProductId")
                        .HasColumnType("citext");

                    b.HasKey("SerialHash", "UserId");

                    b.HasIndex("ProductId");

                    b.HasIndex("UserId");

                    b.ToTable("users_products");
                });

            modelBuilder.Entity("PortalBackend.Models.UserRole", b =>
                {
                    b.Property<int>("RoleId")
                        .HasColumnType("integer");

                    b.Property<int>("UserId")
                        .HasColumnType("integer");

                    b.HasKey("RoleId", "UserId");

                    b.HasIndex("UserId");

                    b.ToTable("user_roles");
                });

            modelBuilder.Entity("PortalBackend.Models.WebglGame", b =>
                {
                    b.Property<int>("WebglGameId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("Link")
                        .HasColumnType("text");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.HasKey("WebglGameId");

                    b.ToTable("webgl_games");
                });

            modelBuilder.Entity("PortalBackend.Models.Children", b =>
                {
                    b.HasBaseType("PortalBackend.Models.BaseUser");

                    b.Property<string>("FIO")
                        .IsRequired()
                        .HasColumnType("text");

                    b.ToTable("users");

                    b.HasDiscriminator().HasValue("Children");
                });

            modelBuilder.Entity("PortalBackend.Models.Kindergarten", b =>
                {
                    b.HasBaseType("PortalBackend.Models.BaseUser");

                    b.Property<string>("KGInn")
                        .IsRequired()
                        .HasMaxLength(64)
                        .HasColumnType("character varying(64)");

                    b.Property<string>("KGName")
                        .IsRequired()
                        .HasMaxLength(254)
                        .HasColumnType("character varying(254)");

                    b.HasIndex("KGInn")
                        .IsUnique();

                    b.ToTable("users");

                    b.HasDiscriminator().HasValue("Kindergarten");
                });

            modelBuilder.Entity("PortalBackend.Models.Parent", b =>
                {
                    b.HasBaseType("PortalBackend.Models.BaseUser");

                    b.Property<string>("City")
                        .IsRequired()
                        .HasMaxLength(64)
                        .HasColumnType("character varying(64)");

                    b.Property<string>("Country")
                        .IsRequired()
                        .HasMaxLength(64)
                        .HasColumnType("character varying(64)");

                    b.Property<string>("FIO")
                        .IsRequired()
                        .HasMaxLength(64)
                        .HasColumnType("character varying(64)")
                        .HasColumnName("Parent_FIO");

                    b.Property<string>("State")
                        .IsRequired()
                        .HasMaxLength(64)
                        .HasColumnType("character varying(64)");

                    b.ToTable("users");

                    b.HasDiscriminator().HasValue("Parent");
                });

            modelBuilder.Entity("PortalBackend.Models.Pedagog", b =>
                {
                    b.HasBaseType("PortalBackend.Models.BaseUser");

                    b.Property<string>("City")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("Pedagog_City");

                    b.Property<string>("Country")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("Pedagog_Country");

                    b.Property<string>("FIO")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("Pedagog_FIO");

                    b.Property<string>("Organization")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Position")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("State")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("Pedagog_State");

                    b.Property<string>("Subjects")
                        .HasColumnType("text");

                    b.ToTable("users");

                    b.HasDiscriminator().HasValue("Pedagog");
                });

            modelBuilder.Entity("PortalBackend.Models.User", b =>
                {
                    b.HasBaseType("PortalBackend.Models.BaseUser");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(254)
                        .HasColumnType("citext");

                    b.Property<bool>("EmailValidate")
                        .HasColumnType("boolean");

                    b.Property<bool>("NumberValidate")
                        .HasColumnType("boolean");

                    b.Property<string>("PasswordHash")
                        .IsRequired()
                        .HasMaxLength(44)
                        .HasColumnType("character varying(44)");

                    b.Property<string>("PhoneNumber")
                        .HasMaxLength(15)
                        .HasColumnType("character varying(15)");

                    b.Property<string>("Salt")
                        .IsRequired()
                        .HasMaxLength(24)
                        .HasColumnType("character varying(24)");

                    b.HasIndex("Email")
                        .IsUnique();

                    b.HasIndex("UserId")
                        .IsUnique();

                    b.ToTable("users");

                    b.HasDiscriminator().HasValue("User");
                });

            modelBuilder.Entity("PortalBackend.Models.ChildrenParent", b =>
                {
                    b.HasOne("PortalBackend.Models.Children", "Children")
                        .WithMany()
                        .HasForeignKey("ChildrenId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("PortalBackend.Models.Parent", "Parent")
                        .WithMany()
                        .HasForeignKey("ParentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Children");

                    b.Navigation("Parent");
                });

            modelBuilder.Entity("PortalBackend.Models.Game", b =>
                {
                    b.HasOne("PortalBackend.Models.Product", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Product");
                });

            modelBuilder.Entity("PortalBackend.Models.GameTag", b =>
                {
                    b.HasOne("PortalBackend.Models.Product", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("PortalBackend.Models.Game", "Game")
                        .WithMany()
                        .HasForeignKey("GameId", "ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Game");

                    b.Navigation("Product");
                });

            modelBuilder.Entity("PortalBackend.Models.GartenPedagog", b =>
                {
                    b.HasOne("PortalBackend.Models.Kindergarten", "Kindergarten")
                        .WithMany()
                        .HasForeignKey("KindergartenUserId");

                    b.HasOne("PortalBackend.Models.Pedagog", "Pedagog")
                        .WithMany()
                        .HasForeignKey("PedagogUserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Kindergarten");

                    b.Navigation("Pedagog");
                });

            modelBuilder.Entity("PortalBackend.Models.ProductEducation", b =>
                {
                    b.HasOne("PortalBackend.Models.Product", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Product");
                });

            modelBuilder.Entity("PortalBackend.Models.RolePermission", b =>
                {
                    b.HasOne("PortalBackend.Models.Role", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Role");
                });

            modelBuilder.Entity("PortalBackend.Models.UserProduct", b =>
                {
                    b.HasOne("PortalBackend.Models.Product", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId");

                    b.HasOne("PortalBackend.Models.BaseUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Product");

                    b.Navigation("User");
                });

            modelBuilder.Entity("PortalBackend.Models.UserRole", b =>
                {
                    b.HasOne("PortalBackend.Models.Role", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("PortalBackend.Models.BaseUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Role");

                    b.Navigation("User");
                });
#pragma warning restore 612, 618
        }
    }
}
