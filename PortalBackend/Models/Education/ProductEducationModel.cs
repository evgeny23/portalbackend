﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalBackend.Models
{
    [Table("product_education")]
    public class ProductEducation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EduId { get; set; }

        [Required]
        public string ProductId { get; set; }

        public Product Product { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(1024)]
        public string EduName { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(8096)]
        public string EduDesc { get; set; }

        [Required]
        [MinLength(10)]
        [MaxLength(256)]
        public string ImageLink { get; set; }

        [Required]
        [Column(TypeName = "citext")]
        public string PageSource { get; set; }
    }
}
