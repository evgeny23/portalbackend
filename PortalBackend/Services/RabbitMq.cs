﻿using RabbitMQ.Client;
using System.Diagnostics;

namespace PortalBackend.Services
{
    public static class Rabbit
    {
        public static IConnection conn;
        public static IModel channel;
        private static ConnectionFactory factory = new ConnectionFactory();

        public static void InitAndDeclare()
        {
            try
            {
                factory.UserName = "rabbit";
                factory.Password = "rabbit";
                factory.VirtualHost = "/";
                factory.HostName = Config.RabbitHost;
                conn = factory.CreateConnection();
                channel = conn.CreateModel();
                channel.ExchangeDeclare("pc", ExchangeType.Direct);
                channel.QueueDeclare("que", false, false, false, null);
                channel.QueueBind("que", "pc", "command");
            } 
            catch
            {
                Debug.WriteLine("RabbitMq Error");
            }
        }

        public static IModel GetChannel()
        {
            if (channel != null && channel.IsOpen)
            {
                return channel;
            }
            InitAndDeclare();
            if (channel != null && channel.IsOpen)
            {
                return channel;
            }
            return null;
        }
    }
}
