﻿using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Responses;
using PortalBackend.Security;

namespace PortalBackend.Controllers.ProductsController
{
    [Route("api/product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        /// <summary>
        /// Добавление нового продукта
        /// [CheckPermissions(PermissionsEnum.Admin)]
        /// </summary>
        /// <response code="200">Добавление успешно</response>
        /// <response code="422">Продукт уже существует</response>
        [HttpPost]
        [CheckPermissions(PermissionsEnum.Admin)]
        public IActionResult Post([FromBody] ProductData data)
        {
            return ProductDB.AddProduct(data).Resp();
        }

        /// <summary>
        /// Обновление существующего продукта
        /// [CheckPermissions(PermissionsEnum.Admin)]
        /// </summary>
        /// <response code="200">Обновление успешно</response>
        [HttpPut]
        [CheckPermissions(PermissionsEnum.Admin)]
        public IActionResult UpdateProduct([FromBody] ProductData data)
        {
            return ProductDB.UpdateProduct(data).Resp();
        }

        /// <summary>
        /// Удаление продукта
        /// [CheckPermissions(PermissionsEnum.Admin)]
        /// </summary>
        /// <response code="200">Удаление успешно</response>
        [HttpDelete("{key}")]
        [CheckPermissions(PermissionsEnum.Admin)]
        public IActionResult DeleteProduct(string key)
        {
            return ProductDB.DeleteProduct(key).Resp(); 
        }

        /// <summary>
        /// Получение продукта по ID
        /// [CheckPermissions(PermissionsEnum.Admin)]
        /// </summary>
        /// <response code="200">Успешно</response>
        [HttpGet("{key}")]
        [CheckPermissions(PermissionsEnum.Admin)]
        public IActionResult GetProductByKey(string key)
        {
            return ProductDB.GetProduct(key).Resp();
        }

        /// <summary>
        /// Получение списка продуктов с пагинацией
        /// [CheckPermissions(PermissionsEnum.Admin)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса. offset/count значения ошибочны</response>
        [HttpGet("{offset}/{count}")]
        [CheckPermissions(PermissionsEnum.Admin)]
        public IActionResult GetAll(int offset = 0, int count = 5)
        {
            if (offset < 0 || count < 1 || count > Config.maxPaginationCountLimit)
            {
                return StandartResponses.BadRequest("Ошибка в offset/count").Resp();
            }
            return ProductDB.GetProducts(offset, count).Resp();
        }
    }
}
