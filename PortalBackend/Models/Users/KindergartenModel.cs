﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalBackend.Models
{
    public class Kindergarten: BaseUser
    {
        [Required]
        [MaxLength(64)]
        [MinLength(5)]
        public string KGInn { get; set; }

        [Required]
        [MaxLength(254)]
        [MinLength(5)]
        public string KGName { get; set; }

        public override string UserName
        {
            get
            {
                return KGName;
            }
        }
    }
}
