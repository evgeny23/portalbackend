﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RemoteControlSignalServer
{
    public static class ConnectedClients
    {
        /// <summary>
        /// md5key: IClientProxy
        /// </summary>
        private static Dictionary<string, IClientProxy> clients = new Dictionary<string, IClientProxy>();
        /// <summary>
        /// b64key: md5key
        /// </summary>
        private static Dictionary<string, string> cacheSerialKeys = new Dictionary<string, string>();
        /// <summary>
        /// md5key: b64key
        /// </summary>
        private static Dictionary<string, string> reverseCacheSerialKeys = new Dictionary<string, string>();

        public static void AddClient(string b64key, string md5key, IClientProxy client)
        {
            if (!clients.ContainsKey(md5key))
            {
                clients.Add(md5key, client);
            } else
            {
                clients[md5key] = client;
            }
            if (!cacheSerialKeys.ContainsKey(b64key))
                cacheSerialKeys.Add(b64key, md5key);
            if (!reverseCacheSerialKeys.ContainsKey(md5key))
                reverseCacheSerialKeys.Add(md5key, b64key);
        }

        public static IClientProxy GetClientBySerialKey(string serialKey)
        {
            if (clients.ContainsKey(serialKey))
                return clients[serialKey];
            return null;
        }

        public static string GetMd5keyByB64Key(string b64key)
        {
            if (cacheSerialKeys.ContainsKey(b64key))
                return cacheSerialKeys[b64key];
            return null;
        }

        public static string GetB64keyByMd5Key(string md5key)
        {
            if (reverseCacheSerialKeys.ContainsKey(md5key))
                return reverseCacheSerialKeys[md5key];
            return null;
        }
    }
}
