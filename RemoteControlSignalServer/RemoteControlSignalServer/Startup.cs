using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using ProductsProtocol;
using RabbitMQ.Client;
using RemoteControlSignalServer.Hubs;
using System;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace RemoteControlSignalServer
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors("CorsPolicy");

            Thread t = new Thread(() => GetTasks());
            t.Start();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<MainHub>("/connect");
            });
        }

        private async void GetTasks()
        {
            while (true)
            {
                Rabbit rabbit = RabbitChannel.GetRabbit();
                if (rabbit == null)
                {
                    Thread.Sleep(100);
                    continue;
                }
                BasicGetResult result = rabbit.channel.BasicGet("que", true);
                if (result == null)
                {
                    Thread.Sleep(100);
                    continue;
                }
                ReadOnlyMemory<byte> body = result.Body;
                string data = Encoding.UTF8.GetString(body.ToArray());
                var command = JsonConvert.DeserializeObject<CommandRunProduct>(data);
                IClientProxy client = ConnectedClients.GetClientBySerialKey(command.serialKey);
                if (client != null)
                {
                    Debug.WriteLine(data);
                    await client.SendAsync("Command", data);
                }
            }
        }
    }
}
