﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBackend.Controllers.RegionsController
{
    public class LocationData
    {
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
    }
}
