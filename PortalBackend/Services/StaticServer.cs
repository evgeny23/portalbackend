﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebDav;

namespace PortalBackend.Services
{
    public static class StaticServer
    {
        private static IWebDavClient _client = new WebDavClient(
            new WebDavClientParams
            {
                BaseAddress = new Uri("http://10.6.6.6:88/"),
                Credentials = new NetworkCredential("dav", "dav")
            }
        );

        public static string UploadFile(string path, string filename, MemoryStream file)
        {
            string fileUri = String.Format("{0}/{1}", path, filename);
            var response = _client.PutFile(fileUri, file);
            return response.Result.ToString();
        }
    }
}
