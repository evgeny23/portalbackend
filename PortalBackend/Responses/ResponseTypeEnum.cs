﻿namespace PortalBackend.Responses
{
    public enum ResponseType
    {
        Data,
        Error,
    }
}
