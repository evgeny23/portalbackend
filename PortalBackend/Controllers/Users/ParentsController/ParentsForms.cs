﻿using System;

namespace PortalBackend.Controllers.ParentsController
{
    public class ParentRegisterData
    {
        public string FIO { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
    }
}
