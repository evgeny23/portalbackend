﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PortalBackend.Models
{
    [Table("children_parents")]
    public class ChildrenParent
    {
        public int ChildrenId { get; set; }
        public Children Children { get; set; }

        public int ParentId { get; set; }
        public Parent Parent { get; set; }
    }
}
