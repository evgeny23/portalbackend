﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;

namespace PortalBackend.Responses
{
    public class BaseResponse
    {
        [JsonIgnore]
        public ResponseType ResponseType { get; set; } = ResponseType.Data;

        [JsonIgnore]
        public int statusCode { get; set; } = 200;

        public string type;

        public string message { get; set; }

        public object data { get; set; }

        private string JsonPrettify(string json)
        {
            using (var stringReader = new StringReader(json))
            using (var stringWriter = new StringWriter())
            {
                var jsonReader = new JsonTextReader(stringReader);
                var jsonWriter = new JsonTextWriter(stringWriter) { Formatting = Formatting.Indented };
                jsonWriter.WriteToken(jsonReader);
                return stringWriter.ToString();
            }
        }

        public string Serialize()
        {
            type = ResponseType.ToString();
            return JsonPrettify(JsonConvert.SerializeObject(this));
        }

        public ObjectResult Resp()
        {
            var obj = new ObjectResult(this.Serialize());
            obj.StatusCode = statusCode;
            return obj;
        }
    }
}
