﻿namespace PortalBackend.Controllers.EducationController
{
    public class EducationProgrammData
    {
        public string ProductId { get; set; }

        public string EduName { get; set; }

        public string EduDesc { get; set; }

        public string ImageLink { get; set; }

        public string PageSource { get; set; }
    }
}
