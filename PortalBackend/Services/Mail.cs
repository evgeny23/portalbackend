﻿using MailKit.Net.Smtp;
using MimeKit;
using PortalBackend.Services.EmailTemplates;

namespace PortalBackend.Services
{
    public static class Mail
    {
        public static void SendActivationEmail(string email, string link)
        {
            string text = EmailTemplatesGenerator.GetEmailActivationTemplate(link);
            SendEmailAsync(email, "Подтверждение регистрации на playstand.ru", text);
        }

        public static void SendRestoreEmail(string email, string link)
        {
            string text = EmailTemplatesGenerator.GetPasswordRestoreTemplate(link);
            SendEmailAsync(email, "Восстановление пароля на playstand.ru", text);
        }

        public static async void SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("noreply@playstand.ru", "noreply@playstand.ru"));
            emailMessage.To.Add(new MailboxAddress(email, email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };
            using (var client = new SmtpClient())
            {
                try
                {
                    await client.ConnectAsync("smtp.yandex.ru", 465, true);
                    await client.AuthenticateAsync("noreply@playstand.ru", "sgxT7D6pxcSimeX");
                    await client.SendAsync(emailMessage);
                    await client.DisconnectAsync(true);
                }
                catch { }
            }
        }
    }
}
