﻿using System.ComponentModel.DataAnnotations;

namespace PortalBackend.Models
{
    public class Parent: BaseUser
    {
        [Required]
        [MaxLength(64)]
        [MinLength(6)]
        public string FIO { get; set; }

        [Required]
        [MaxLength(64)]
        public string Country { get; set; }

        [Required]
        [MaxLength(64)]
        public string State { get; set; }

        [Required]
        [MaxLength(64)]
        public string City { get; set; }

        public override string UserName
        {
            get
            {
                return FIO;
            }
        }
    }
}
