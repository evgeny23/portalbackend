﻿using PortalBackend.Models;
using PortalBackend.Responses;
using ProductsProtocol;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PortalBackend.Controllers.UserProductController
{
    public static class DbMethods
    {
        internal static BaseResponse AddNewProductToUser(int userId, string key)
        {
            string productKey = KeyCheckWcf.KeyChec(key);
            if (productKey == null)
            {
                return StandartResponses.BadRequest("Введён неверный ключ");
            }
            string hash = CreateMD5(key);
            using (ApplicationContext db = new ApplicationContext())
            {
                if (db.UserProducts.Where( up => up.SerialHash == hash).Count() > 0)
                {
                    return StandartResponses.BadRequest("Ключ уже был добавлен");
                }
                db.UserProducts.Add(new UserProduct() { ProductId = productKey, SerialHash = hash, UserId = userId });
                db.SaveChanges();
                return StandartResponses.Ok("Ключ успешно добавлен");
            }
        }

        internal static BaseResponse GetProductBySerialKey(int userId, string serialKeyHash)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                UserProduct product = db.UserProducts
                    .OrderByDescending(x => x.UserId == userId && x.SerialHash == serialKeyHash)
                    .FirstOrDefault();
                if (product == null)
                {
                    return StandartResponses.BadRequest("Неверный идентификатор продукта");
                }
                return StandartResponses.Ok("Продукт пользователя", product);
            }
        }

        internal static BaseResponse GetProducts(int userId, int offset, int count)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                List<UserProduct> products = db.UserProducts
                    .Where(p => p.UserId == userId)
                    .OrderByDescending(x => x.UserId == userId)
                    .Skip(offset)
                    .Take(count)
                    .ToList();
                return StandartResponses.Ok("Список продуктов пользователя", products);
            }
        }

        public static BaseResponse AddCommandRunProduct(int userId, string serialKeyHash)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                (UserProduct product, BaseResponse Response) = CheckProductOnline(userId, serialKeyHash);
                if (Response != null)
                    return Response;
                CommandRunProduct command = new CommandRunProduct()
                {
                    commandType = CommandType.runProduct,
                    productCode = product.ProductId,
                    serialKey = serialKeyHash,
                };
                RabbitSend.AddCommand(command);
                return StandartResponses.Ok("Команда добавлена в очередь");
            }
        }

        public static BaseResponse AddCommandSetVolume(int userId, string serialKeyHash, int volume)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                (UserProduct product, BaseResponse Response) = CheckProductOnline(userId, serialKeyHash);
                if (Response != null)
                    return Response;
                CommandVolume command = new CommandVolume()
                {
                    commandType = CommandType.runProduct,
                    volume = volume,
                    serialKey = serialKeyHash,
                };
                RabbitSend.AddCommand(command);
                return StandartResponses.Ok("Команда добавлена в очередь");
            }
        }

        public static BaseResponse AddCommandRunGame(int userId, string serialKeyHash, int gameId)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                (UserProduct product, BaseResponse Response) = CheckProductOnline(userId, serialKeyHash, true);
                if (Response != null)
                    return Response;
                CommandRunGame command = new CommandRunGame()
                {
                    commandType = CommandType.runProduct,
                    gameCode = gameId,
                    productCode = product.ProductId,
                    serialKey = serialKeyHash,
                };
                RabbitSend.AddCommand(command);
                return StandartResponses.Ok("Команда добавлена в очередь");
            }
        }

        private static (UserProduct, BaseResponse) CheckProductOnline(int userId, string serialKeyHash, bool isRun = false)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                UserProduct product = db.UserProducts.Where(up => up.UserId == userId && up.SerialHash == serialKeyHash).FirstOrDefault();
                if (product == null)
                {
                    return (product, StandartResponses.BadRequest("Этот продукт не принадлежит данному пользователю"));
                }
                if (!product.IsOnline)
                {
                    return (product, StandartResponses.BadRequest("Этот продукт оффлайн"));
                }
                if (isRun && !product.IsRun)
                {
                    return (product, StandartResponses.BadRequest("Продукт выключен"));
                }
                return (product, null);
            }
        }

        private static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}
