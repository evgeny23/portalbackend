﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalBackend.Migrations
{
    public partial class users_save : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "FIO",
                table: "users",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Parent_FIO",
                table: "users",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "children_parents",
                columns: table => new
                {
                    ChildrenId = table.Column<int>(type: "integer", nullable: false),
                    ParentId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_children_parents", x => new { x.ChildrenId, x.ParentId });
                    table.ForeignKey(
                        name: "FK_children_parents_users_ChildrenId",
                        column: x => x.ChildrenId,
                        principalTable: "users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_children_parents_users_ParentId",
                        column: x => x.ParentId,
                        principalTable: "users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "gartens_pedagogs",
                columns: table => new
                {
                    GartenUserId = table.Column<int>(type: "integer", nullable: false),
                    PedagogUserId = table.Column<int>(type: "integer", nullable: false),
                    KindergartenUserId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gartens_pedagogs", x => new { x.GartenUserId, x.PedagogUserId });
                    table.ForeignKey(
                        name: "FK_gartens_pedagogs_users_KindergartenUserId",
                        column: x => x.KindergartenUserId,
                        principalTable: "users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gartens_pedagogs_users_PedagogUserId",
                        column: x => x.PedagogUserId,
                        principalTable: "users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "roles",
                columns: new[] { "RoleId", "Custom", "Name" },
                values: new object[] { 5, false, "Педагог" });

            migrationBuilder.InsertData(
                table: "role_permissions",
                columns: new[] { "Permission", "RoleId" },
                values: new object[,]
                {
                    { 1, 5 },
                    { 2, 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_children_parents_ParentId",
                table: "children_parents",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_gartens_pedagogs_KindergartenUserId",
                table: "gartens_pedagogs",
                column: "KindergartenUserId");

            migrationBuilder.CreateIndex(
                name: "IX_gartens_pedagogs_PedagogUserId",
                table: "gartens_pedagogs",
                column: "PedagogUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "children_parents");

            migrationBuilder.DropTable(
                name: "gartens_pedagogs");

            migrationBuilder.DeleteData(
                table: "role_permissions",
                keyColumns: new[] { "Permission", "RoleId" },
                keyValues: new object[] { 1, 5 });

            migrationBuilder.DeleteData(
                table: "role_permissions",
                keyColumns: new[] { "Permission", "RoleId" },
                keyValues: new object[] { 2, 5 });

            migrationBuilder.DeleteData(
                table: "roles",
                keyColumn: "RoleId",
                keyValue: 5);

            migrationBuilder.DropColumn(
                name: "Parent_FIO",
                table: "users");

            migrationBuilder.AlterColumn<string>(
                name: "FIO",
                table: "users",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);
        }
    }
}
