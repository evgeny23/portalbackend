﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace PortalBackend.Migrations
{
    public partial class education : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "edu_courses",
                columns: table => new
                {
                    EduId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(512)", maxLength: 512, nullable: false),
                    Desc = table.Column<string>(type: "character varying(8096)", maxLength: 8096, nullable: false),
                    Image = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    PageSource = table.Column<string>(type: "citext", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_edu_courses", x => x.EduId);
                });

            migrationBuilder.CreateTable(
                name: "product_education",
                columns: table => new
                {
                    EduId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ProductId = table.Column<int>(type: "integer", nullable: false),
                    ProductId1 = table.Column<string>(type: "citext", nullable: true),
                    EduName = table.Column<string>(type: "character varying(1024)", maxLength: 1024, nullable: false),
                    EduDesc = table.Column<string>(type: "character varying(8096)", maxLength: 8096, nullable: false),
                    ImageLink = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    PageSource = table.Column<string>(type: "citext", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_education", x => x.EduId);
                    table.ForeignKey(
                        name: "FK_product_education_products_ProductId1",
                        column: x => x.ProductId1,
                        principalTable: "products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_product_education_ProductId1",
                table: "product_education",
                column: "ProductId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "edu_courses");

            migrationBuilder.DropTable(
                name: "product_education");
        }
    }
}
