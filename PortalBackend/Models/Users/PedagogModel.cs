﻿using System.ComponentModel.DataAnnotations;

namespace PortalBackend.Models
{
    public class Pedagog: BaseUser
    {
        [Required]
        public string FIO { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string State { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Organization { get; set; }
        [Required]
        public string Position { get; set; }

        public override string UserName
        {
            get
            {
                return FIO;
            }
        }
    }
}
