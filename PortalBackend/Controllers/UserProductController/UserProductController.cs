using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Responses;
using PortalBackend.Security;
using System;

namespace PortalBackend.Controllers.UserProductController
{
    [Route("api/user_products")]
    [ApiController]
    public class UserProductController : ControllerBase
    {
        /// <summary>
        /// Добавление ключа продукта
        /// [CheckPermissions(PermissionsEnum.Common)]
        /// </summary>
        /// <param name="data">UserProductData</param>
        /// <response code="200">Ключ валидирован и добавлен</response>
        /// <response code="400">Ошибка при добавлении ключа</response>
        [HttpPost]
        [CheckPermissions(PermissionsEnum.Common)]
        public IActionResult AddNewProduct([FromBody] UserProductData data)
        {
            int userId = Convert.ToInt32(JwtTokens.GetValueFromPayload(Request.Headers, "userId"));
            return DbMethods.AddNewProductToUser(userId, data.key).Resp();
        }

        /// <summary>
        /// Получения продукта пользователя
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">идентификатор пользователя</param>
        /// <param name="serialKeyHash">Hash ключа продукта</param>
        /// <response code="200">Список продуктов пользователя предоставлен</response>
        /// <response code="400">Неверный идентификатор продукта</response>
        [HttpGet("product/{userId}/{serialKeyHash}")]
        [CheckPermissions(PermissionsEnum.Common, "userId")]
        public IActionResult GetProductByHash(int userId, string serialKeyHash)
        {
            return DbMethods.GetProductBySerialKey(userId, serialKeyHash).Resp();
        }

        /// <summary>
        /// Получение списка продуктов пользователя с пагинацией
        /// [CheckPermissions(PermissionsEnum.Common)]
        /// </summary>
        /// <param name="offset">Смещение</param>
        /// <param name="count">Количество продуктов</param>
        /// <response code="200">Список продуктов пользователя предоставлен</response>
        /// <response code="400">Ошибка в offset или count</response>
        [HttpGet("list/{userId}/{offset}/{count}")]
        [CheckPermissions(PermissionsEnum.Common)]
        public IActionResult GetProducts(int offset, int count)
        {
            if (offset < 0 || count < 1 || count > Config.maxPaginationCountLimit)
            {
                return StandartResponses.BadRequest("Неверные offset/count").Resp();
            }
            int userId = Convert.ToInt32(JwtTokens.GetValueFromPayload(Request.Headers, "userId"));
            return DbMethods.GetProducts(userId, offset, count).Resp();
        }

        /// <summary>
        /// Запуск пользовательского продукта
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">id владельца ключа</param>
        /// <param name="serialKeyHash">Хэш серийного ключа</param>
        /// <response code="200">Команда добавлена в очередь</response>
        /// <response code="400">Ошибка при добавлении команды</response>
        /// <response code="406">Продукт оффлайн</response>
        [HttpGet("run_product/{userId}/{serialKeyHash}")]
        [CheckPermissions(PermissionsEnum.Common, "userId")]
        public IActionResult RunProduct(int userId, string serialKeyHash)
        {
            return DbMethods.AddCommandRunProduct(userId, serialKeyHash).Resp();
        }

        /// <summary>
        /// Прибавить громкость
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">id владельца ключа</param>
        /// <param name="serialKeyHash">Хэш серийного ключа</param>
        /// <response code="200">Команда добавлена в очередь</response>
        /// <response code="400">Ошибка при добавлении команды</response>
        [HttpGet("volume_plus/{userId}/{serialKeyHash}")]
        [CheckPermissions(PermissionsEnum.Common, "userId")]
        public IActionResult VolumePlus(int userId, string serialKeyHash)
        {
            return DbMethods.AddCommandSetVolume(userId, serialKeyHash, 5).Resp();
        }

        /// <summary>
        /// Убавить громкость
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">id владельца ключа</param>
        /// <param name="serialKeyHash">Хэш серийного ключа</param>
        /// <response code="200">Команда добавлена в очередь</response>
        /// <response code="400">Ошибка при добавлении команды</response>
        [HttpGet("volume_minus/{userId}/{serialKeyHash}")]
        [CheckPermissions(PermissionsEnum.Common, "userId")]
        public IActionResult VolumeMinus(int userId, string serialKeyHash)
        {
            return DbMethods.AddCommandSetVolume(userId, serialKeyHash, -5).Resp();
        }

        /// <summary>
        /// Запустить игру
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">id владельца ключа</param>
        /// <param name="serialKeyHash">Хэш серийного ключа</param>
        /// <param name="gameId">Идентификатор игры</param>
        /// <response code="200">Команда добавлена в очередь</response>
        /// <response code="400">Ошибка при добавлении команды</response>
        [HttpGet("run_game/{userId}/{serialKeyHash}/{gameId}")]
        [CheckPermissions(PermissionsEnum.Common, "userId")]
        public IActionResult RunGame(int userId, string serialKeyHash, int gameId)
        {
            return DbMethods.AddCommandRunGame(userId, serialKeyHash, gameId).Resp();
        }
    }
}
