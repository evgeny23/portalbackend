﻿using PortalBackend.Models;
using PortalBackend.Responses;
using System.Collections.Generic;
using System.Linq;

namespace PortalBackend.Controllers.WebglGamesController
{
    public class DbMethods
    {
        internal static BaseResponse CreateNewGame(AddGameData data)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                int cnt = db.WebglGames
                    .Where(wg => wg.Name.ToLower() == data.name.ToLower() || wg.Link.ToLower() == data.path.ToLower())
                    .Count();
                if (cnt > 0)
                {
                    return StandartResponses.BadRequest("Игра уже существует");
                }

                WebglGame newGame = new WebglGame();
                newGame.Name = data.name;
                newGame.Link = data.path;
                db.WebglGames.Add(newGame);
                db.SaveChanges();
                return StandartResponses.Ok("Игра успешно добавлена", newGame);
            }
        }

        internal static BaseResponse GetListOfGames(int offset, int count)
        {
            List<WebglGame> games;
            using (ApplicationContext db = new ApplicationContext())
            {
                games = db.WebglGames.OrderBy(wg => wg.WebglGameId).Skip(offset).Take(count).ToList();
            }
            return StandartResponses.Ok("Список игр", games);
        }

        internal static BaseResponse GetWebglGameById(int gameId)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                WebglGame game = db.WebglGames.Where(wg => wg.WebglGameId == gameId).FirstOrDefault();
                if (game == null)
                {
                    return new BaseResponse()
                    {
                        ResponseType = ResponseType.Error,
                        message = "Такой игры не существует",
                        statusCode = 400,
                    };
                }
                return StandartResponses.BadRequest("Запрашиваемая игра", game);
            }
        }
    }
}
