﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalBackend.Migrations
{
    public partial class delete_login : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_users_Login",
                table: "users");

            migrationBuilder.DropColumn(
                name: "Login",
                table: "users");

            migrationBuilder.RenameColumn(
                name: "ParentSName",
                table: "users",
                newName: "State");

            migrationBuilder.RenameColumn(
                name: "ParentFName",
                table: "users",
                newName: "FIO");

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "users",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "users",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organization",
                table: "users",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Pedagog_City",
                table: "users",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Pedagog_Country",
                table: "users",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Pedagog_FIO",
                table: "users",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Pedagog_State",
                table: "users",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Position",
                table: "users",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Subjects",
                table: "users",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "users");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "users");

            migrationBuilder.DropColumn(
                name: "Organization",
                table: "users");

            migrationBuilder.DropColumn(
                name: "Pedagog_City",
                table: "users");

            migrationBuilder.DropColumn(
                name: "Pedagog_Country",
                table: "users");

            migrationBuilder.DropColumn(
                name: "Pedagog_FIO",
                table: "users");

            migrationBuilder.DropColumn(
                name: "Pedagog_State",
                table: "users");

            migrationBuilder.DropColumn(
                name: "Position",
                table: "users");

            migrationBuilder.DropColumn(
                name: "Subjects",
                table: "users");

            migrationBuilder.RenameColumn(
                name: "State",
                table: "users",
                newName: "ParentSName");

            migrationBuilder.RenameColumn(
                name: "FIO",
                table: "users",
                newName: "ParentFName");

            migrationBuilder.AddColumn<string>(
                name: "Login",
                table: "users",
                type: "citext",
                maxLength: 64,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_users_Login",
                table: "users",
                column: "Login",
                unique: true);
        }
    }
}
