﻿namespace PortalBackend.Controllers.UsersController
{
    public class RegisterData
    {
        public string email { get; set; }
        public string password { get; set; }
        public string phoneNumber { get; set; }
    }
    
    public class UpdatePasswordData
    {
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
    }

    public class RestorePasswordEmailData
    {
        public string email { get; set; }
    }

    public class NewPasswordData
    {
        public string password { get; set; }
    }
}
