using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using PortalBackend.Models;
using PortalBackend.Security;

namespace PortalBackend
{
    public class ApplicationContext : DbContext
    {
        public DbSet<BaseUser> AllUsers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Kindergarten> Gartens { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<UserProduct> UserProducts { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<GameTag> GameTags { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Pedagog> Pedagogs { get; set; }
        public DbSet<GartenPedagog> GartenPedagogs { get; set; }
        public DbSet<Children> Childrens { get; set; }
        public DbSet<ChildrenParent> ChildrenParents { get; set; }
        public DbSet<ProductEducation> ProductEducations { get; set; }
        public DbSet<EduCourse> EduCourses { get; set; }

        public DbSet<WebglGame> WebglGames { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host="+Config.DbHost+";Port=5432;Database=portal;Username=portal;Password=portal");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BaseUser>().ToTable("users");
            modelBuilder.Entity<Kindergarten>().HasIndex(u => u.KGInn).IsUnique();
            modelBuilder.HasPostgresEnum<PermissionsEnum>();
            modelBuilder.Entity<Role>().HasData(DefaultRoles.roles);
            modelBuilder.Entity<RolePermission>().HasData(DefaultRoles.permissions);
            modelBuilder.Entity<User>()
                .HasIndex(p => new { p.UserId })
                .IsUnique(true);
            modelBuilder.Entity<User>()
                .HasIndex(p => new { p.Email })
                .IsUnique(true);
            modelBuilder.Entity<User>()
                .Property(p =>  p.UserId)
                .IsRequired(true);
            modelBuilder.Entity<User>()
                .Property(p => p.Email)
                .IsRequired(true);
            modelBuilder.Entity<User>()
                .Property(p => p.PasswordHash)
                .IsRequired(true);
            modelBuilder.Entity<User>()
                .Property(p => p.Salt )
                .IsRequired(true);
            modelBuilder.Entity<User>()
                .Property(p => p.RegistrationDate)
                .IsRequired(true);
            modelBuilder.Entity<Role>();
            modelBuilder.Entity<RolePermission>().HasKey(p => new { p.Permission, p.RoleId}); ;
            modelBuilder.Entity<UserRole>().HasKey(p => new { p.RoleId, p.UserId });
            modelBuilder.Entity<UserProduct>().HasKey(p => new { p.SerialHash, p.UserId });
            modelBuilder.Entity<GameTag>().HasKey(gt => new { gt.GameId, gt.ProductId, gt.Tag });
            modelBuilder.Entity<Game>().HasKey(g => new { g.GameId, g.ProductId });
            modelBuilder.Entity<Region>().HasKey(r => new { r.Country, r.State, r.City });
            modelBuilder.Entity<GartenPedagog>().HasKey(gp => new { gp.GartenUserId, gp.PedagogUserId });
            modelBuilder.Entity<ChildrenParent>().HasKey(cp => new { cp.ChildrenId, cp.ParentId });
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls("http://0.0.0.0:5000");
                });
    }
}
