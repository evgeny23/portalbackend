﻿using PortalBackend.Models;
using PortalBackend.Responses;
using PortalBackend.Security;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PortalBackend.Controllers.GartensController
{
    public static class GartensDbMethods
    {
        internal static BaseResponse CreateNewGarten(RegisterGartenData data, int parentUserId)
        {
            ApplicationContext db = new ApplicationContext();
            var Response = ValidateGartenData(data);
            if (Response != null)
                return Response;
        
            if (db.Gartens.Where(g => g.KGInn == data.INN).FirstOrDefault() != null)
            {
                return StandartResponses.BadRequest("Детский сад уже существует");
            }

            Kindergarten garten = new Kindergarten()
            {
                KGInn = data.INN,
                KGName = data.Name,
                ParentUserId = parentUserId,
                RegistrationDate = DateTime.Now,
            };
            db.Gartens.AddRange(garten);
            db.SaveChanges();
            DefaultRoles.SetUserRole(garten.UserId, DefaultRolesEnum.Garten);
            return StandartResponses.Ok("Пользователь успешно зарегистрирован", garten);
        }

        internal static BaseResponse ValidateGartenData(RegisterGartenData data)
        {
            if (!Validators.IsValidInn(data.INN))
                return StandartResponses.BadRequest("Некорректный ИНН");
            if (data.Name.Length < 6)
                return StandartResponses.BadRequest("Слишком короткое название");
            return null;
        }

        internal static BaseResponse UpdateGartenData(int id, RegisterGartenData data)
        {
            ApplicationContext db = new ApplicationContext();
            Kindergarten garten = db.Gartens.Where(g => g.UserId == id).FirstOrDefault();
            if (garten == null)
                return StandartResponses.BadRequest("Ошибка при обновлении");
            if (db.Gartens.Where(g => g.KGInn == data.INN).FirstOrDefault() != null)
                return StandartResponses.BadRequest("Организация с таким INN уже существует");
            garten.KGInn = data.INN;
            garten.KGName = data.Name;
            db.Gartens.Update(garten);
            db.SaveChanges();
            return StandartResponses.Ok("Данные успешно обновлены", garten);
        }

        internal static BaseResponse GetGartenPedagogsWithPagination(int gartenId, int count, int offset)
        {
            ApplicationContext db = new ApplicationContext();
            List<Pedagog> pedagogs = db.GartenPedagogs.Where(gp => gp.GartenUserId == gartenId)
                .Select(gp => gp.Pedagog)
                .Skip(offset)
                .Take(count).ToList();
            return StandartResponses.Ok("Список п едагогов садика", pedagogs);
        }

        internal static BaseResponse AddPedagog(int gartenId, int pedagogId)
        {
            ApplicationContext db = new ApplicationContext();
            List<BaseUser> users = db.AllUsers.Where(au => au.UserId == gartenId || au.UserId == pedagogId).ToList();
            Kindergarten garten = users.Where(u => u.UserId == gartenId).FirstOrDefault() as Kindergarten;
            Pedagog pedagog = users.Where(u => u.UserId == pedagogId).FirstOrDefault() as Pedagog;
            db.GartenPedagogs.Add(
                new GartenPedagog()
                {
                    GartenUserId = gartenId,
                    PedagogUserId = pedagogId,
                }
            );
            db.SaveChanges();
            return StandartResponses.Ok("Педагог добавлен к дет.саду");
        }

        internal static BaseResponse RemovePedagogFromGarten(int gartenId, int pedagogId)
        {
            ApplicationContext db = new ApplicationContext();
            GartenPedagog gartenPedagog = db.GartenPedagogs.Where(gp => gp.GartenUserId == gartenId && gp.PedagogUserId == pedagogId).FirstOrDefault();
            db.GartenPedagogs.Remove(gartenPedagog);
            db.SaveChanges();
            return StandartResponses.Ok("Педагог отвязан от дет.сада");
        }
    }
}
