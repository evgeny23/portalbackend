﻿using PortalBackend.Models;
using PortalBackend.Responses;
using PortalBackend.Security;
using System;
using System.Linq;

namespace PortalBackend.Controllers.ParentsController
{
    public static class ParentsDbMethods
    {
        internal static BaseResponse CreateNewParent(ParentRegisterData data, int userId)
        {
            ApplicationContext db = new ApplicationContext();
            Parent parent = new Parent()
            {
                ParentUserId = userId,
                FIO = data.FIO,
                Country = data.Country,
                State = data.State,
                City = data.City,
                RegistrationDate = DateTime.Now,
                DefaultAccount = false,
            };
            db.Parents.Add(parent);
            db.SaveChanges();
            DefaultRoles.SetUserRole(parent.UserId, DefaultRolesEnum.Parent);
            return StandartResponses.Ok("Успешно", parent);
        }

        internal static BaseResponse UpdateParent(ParentRegisterData data, int userId)
        {
            ApplicationContext db = new ApplicationContext();
            Parent parent = db.Parents.Where(p => p.UserId == userId).FirstOrDefault();
            parent.FIO = data.FIO;
            parent.Country = data.Country;
            parent.City = data.City;
            parent.State = data.State;
            db.Parents.Update(parent);
            db.SaveChanges();
            return StandartResponses.Ok("Успешно", parent);
        }
    }
}
