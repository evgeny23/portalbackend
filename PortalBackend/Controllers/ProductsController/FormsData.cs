﻿namespace PortalBackend.Controllers.ProductsController
{
    public class ProductData
    {
        public string productId { get; set; }
        public string productName { get; set; }
    }
}
