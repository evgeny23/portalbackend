﻿using Microsoft.AspNetCore.Mvc;

namespace PortalBackend.Responses
{
    public static class StandartResponses
    {
        public static BaseResponse Ok(string message = "", object data = null)
        {
            return new BaseResponse()
            {
                statusCode = 200,
                message = message,
                ResponseType = ResponseType.Data,
                data = data,
            };
        }

        public static BaseResponse BadRequest(string message = "", object data = null)
        {
            return new BaseResponse()
            {
                statusCode = 400,
                message = message,
                ResponseType = ResponseType.Error,
                data = data,
            };
        }

        public static BaseResponse Unauthorized(string message = "", object data = null)
        {
            return new BaseResponse()
            {
                statusCode = 401,
                message = message,
                ResponseType = ResponseType.Error,
                data = data,
            };
        }

        public static BaseResponse Forbidden(string message = "", object data = null)
        {
            return new BaseResponse()
            {
                statusCode = 403,
                message = message,
                ResponseType = ResponseType.Error,
                data = data,
            };
        }
    }
}
