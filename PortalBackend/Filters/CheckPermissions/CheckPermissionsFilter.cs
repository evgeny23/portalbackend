﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using PortalBackend.Controllers.UsersController;
using PortalBackend.Models;
using PortalBackend.Responses;
using PortalBackend.Security;
using System;

namespace PortalBackend.Filters.CheckPermissions
{
    public class CheckPermissionsFilter : IAuthorizationFilter
    {
        PermissionsEnum _permission;
        string _userId;
        bool _userIdCheck;
        bool _ignoreEmailValidate;

        public CheckPermissionsFilter(PermissionsEnum permission, string userId, bool ignoreEmailValidate)
        {
            _userId = userId;
            _permission = permission;
            _ignoreEmailValidate = ignoreEmailValidate;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            StringValues jwtToken;
            if (!context.HttpContext.Request.Headers.TryGetValue("Authorization", out jwtToken))
            {
                context.Result = StandartResponses.Unauthorized().Resp();
                return;
            }
            bool isAdmin;
            bool isPermission;
            try
            {
                isAdmin = JwtTokens.VerifyAndGetPermissionsList(jwtToken).Contains(PermissionsEnum.Admin);
                isPermission = JwtTokens.VerifyAndGetPermissionsList(jwtToken).Contains(_permission);
            }
            catch
            {
                context.Result = StandartResponses.Unauthorized().Resp();
                return;
            }

            int currentUserId = Convert.ToInt32(JwtTokens.GetValueFromPayload(context.HttpContext.Request.Headers, "userId"));
            bool emailActive = Boolean.Parse(JwtTokens.GetValueFromPayload(context.HttpContext.Request.Headers, "email_active").ToString());

            if (!emailActive && !_ignoreEmailValidate)
            {
                context.Result = StandartResponses.BadRequest("Требуется подтверждение email").Resp();
                return;
            }

            if (_userId.Length > 0)
            {
                try
                {
                    int needUserId = Convert.ToInt32(context.HttpContext.Request.RouteValues[_userId]);
                    _userIdCheck = currentUserId == needUserId;
                } 
                catch
                {
                    context.Result = StandartResponses.Unauthorized().Resp();
                    return;
                }
            }
            else
            {
                _userIdCheck = true;
            }

            bool forbidden = isAdmin || (isPermission && _userIdCheck);
            if (!forbidden)
            {
                context.Result = StandartResponses.Forbidden().Resp();
                return;
            }
        }
    }
}
