﻿using PortalBackend.Models;
using PortalBackend.Responses;
using System.Collections.Generic;
using System.Linq;

namespace PortalBackend.Controllers.RegionsController
{
    public static class RegionsDbMethods
    {
        internal static BaseResponse AddNewLocation(LocationData data)
        {
            ApplicationContext db = new ApplicationContext();
            Region region = new Region()
            {
                Country = data.Country,
                State = data.State,
                City = data.City
            };
            db.Add(region);
            db.SaveChanges();
            return StandartResponses.Ok("Добавлено успешно", region);
        }

        internal static BaseResponse GetAllData()
        {
            ApplicationContext db = new ApplicationContext();
            List<Region> data = db.Regions.ToList();
            return StandartResponses.Ok("Все страны и города", data);
        }

        internal static BaseResponse GetCountries()
        {
            ApplicationContext db = new ApplicationContext();
            List<string> countries = db.Regions.Select(r => r.Country).Distinct().ToList();
            return StandartResponses.Ok("Список стран", countries);
        }

        internal static BaseResponse GetStates(string country)
        {
            ApplicationContext db = new ApplicationContext();
            List<string> states = db.Regions.Where(r => r.Country == country).Select(r => r.State).Distinct().ToList();
            return StandartResponses.Ok("Список регионов", states);
        }

        internal static BaseResponse GetCities(string country, string state)
        {
            ApplicationContext db = new ApplicationContext();
            List<string> states = db.Regions.Where(r => r.Country == country).Select(r => r.City).Distinct().ToList();
            return StandartResponses.Ok("Список регионов", states);
        }
    }
}
