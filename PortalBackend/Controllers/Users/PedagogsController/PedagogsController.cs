﻿using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Responses;
using PortalBackend.Security;
using PortalBackend.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBackend.Controllers.Users.PedagogsController
{
    [Route("api/pedagogs")]
    [ApiController]
    public class PedagogsController: ControllerBase
    {
        /// <summary>
        /// Регистрация педагога
        /// [CheckPermissions(PermissionsEnum.Common, "", true)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса</response>
        [HttpPost]
        [CheckPermissions(PermissionsEnum.Common, "", true)]
        public IActionResult Post([FromBody] PedagogData data)
        {
            int userId = CommonDbMethods.GetMasterUserId(Convert.ToInt32(JwtTokens.GetValueFromPayload(Request.Headers, "userId")));
            if (userId == -1)
                return StandartResponses.BadRequest("Ошибка при создании аккаунта").Resp();
            return PedagogsDbMethods.CreateNewPedagog(data, userId).Resp();
        }

        /// <summary>
        /// Обновление педагога
        /// [CheckPermissions(PermissionsEnum.Common, "userId", true)]
        /// </summary>
        /// <param name="data">PedagogData</param>
        /// <param name="userId">id пользователя</param>
        /// <returns></returns>
        [HttpPut("pedagog_update/{userId}")]
        [CheckPermissions(PermissionsEnum.Common, "userId", true)]
        public IActionResult UpdateParent([FromBody] PedagogData data, int userId)
        {
            return PedagogsDbMethods.UpdatePedagog(data, userId).Resp();
        }
    }
}
