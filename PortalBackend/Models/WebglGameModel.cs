﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalBackend.Models
{
    [Table("webgl_games")]
    public class WebglGame
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WebglGameId { get; set; }

        [MinLength(3)]
        public string Name { get; set; }

        [MinLength(5)]
        public string Link { get; set; }
    }
}
