﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBackend.Models
{
    [Table("gartens_pedagogs")]
    public class GartenPedagog
    {
        public int GartenUserId { get; set; }
        public Kindergarten Kindergarten { get; set; }

        public int PedagogUserId { get; set; }
        public Pedagog Pedagog { get; set; }
    }
}
