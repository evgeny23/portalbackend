﻿using PortalBackend.Security;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalBackend.Models
{
    [Table("role_permissions")]
    public class RolePermission
    {
        public PermissionsEnum Permission { get; set; }

        public int RoleId { get; set; }
        public Role Role { get; set; }
    }
}
