﻿using System;

namespace RemoteControlSignalServer
{
    public class Config
    {
        private static string RedisHostDev = "127.0.0.1";
        private static string RedisHostProd = Environment.GetEnvironmentVariable("REDIS_HOST");

        public static string RedisHost
        {
            get
            {
                if (RedisHostProd == null)
                {
                    return RedisHostDev;
                }
                else
                {
                    return RedisHostProd;
                }
            }
        }

        private static string RabbitHostDev = "127.0.0.1";
        private static string RabbitHostProd = Environment.GetEnvironmentVariable("RABBIT_HOST");

        public static string RabbitHost
        {
            get
            {
                if (RabbitHostProd == null)
                {
                    return RabbitHostDev;
                }
                else
                {
                    return RabbitHostProd;
                }
            }
        }
    }
}
