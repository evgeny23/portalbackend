﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalBackend.Models
{
    [Table("games_tags")]
    public class GameTag
    {
        [Column(TypeName = "citext")]
        public string Tag { get; set; }

        public int GameId { get; set; }
        [JsonIgnore]
        public Game Game { get; set; }

        public string ProductId { get; set; }
        public Product Product { get; set; }
    }
}
