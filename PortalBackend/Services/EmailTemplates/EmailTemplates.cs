﻿using System.IO;

namespace PortalBackend.Services.EmailTemplates
{
    public static class EmailTemplatesGenerator
    {
        private const string emailActivationPath = @"Services/EmailTemplates/templates/email_activation.html";
        private static string _emailActivationTemaplte = File.ReadAllText(emailActivationPath);

        public static string GetEmailActivationTemplate(string link)
        {
            string activationTemplate = _emailActivationTemaplte;
            return activationTemplate.Replace("$link", link);
        }


        private const string passwordRestorePath = @"Services/EmailTemplates/templates/password_restore.html";
        private static string _passwordRestoreTemplate = File.ReadAllText(passwordRestorePath);

        public static string GetPasswordRestoreTemplate(string link)
        {
            string activationTemplate = _passwordRestoreTemplate;
            return activationTemplate.Replace("$link", link);
        }
    }
}
