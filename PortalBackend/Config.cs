﻿using System;

namespace PortalBackend
{
    public enum WorkMode
    {
        LOCAL,
        DEVELOPMENT,
        PRODUCTION,
    }

    public enum RedisDatabases
    {
        OAuth = 0,
        OnlineProducts = 1,
        ActivationCodes = 2,
    }

    public static class Config
    {
        public static string siteHostLocal = "http://localhost:51350";
        public static string siteHostDev = Environment.GetEnvironmentVariable("SITE_HOST");
        public static string siteHostProd = Environment.GetEnvironmentVariable("SITE_HOST");

        public static string defaultAvatar = "http://10.6.6.6:88/static/avatars/default.jpg";

        public static string SiteHost
        {
            get
            {
                switch (GetMode())
                {
                    case WorkMode.PRODUCTION:
                        return siteHostProd;
                    case WorkMode.DEVELOPMENT:
                        return siteHostDev;
                    case WorkMode.LOCAL:
                        return siteHostLocal;
                    default:
                        return siteHostLocal;
                }
            }
        }

        public static int maxPaginationCountLimit = 200;

        public static WorkMode mode = GetMode();

        private static string RedisHostDev = "127.0.0.1";
        private static string RedisHostProd = Environment.GetEnvironmentVariable("REDIS_HOST");

        public static string RedisHost 
        { 
            get 
            { 
                if(RedisHostProd == null)
                {
                    return RedisHostDev;
                }
                else
                {
                    return RedisHostProd;
                }
            }
        }

        private static string DbHostDev = "127.0.0.1";
        private static string DbHostProd = Environment.GetEnvironmentVariable("DB_HOST");

        public static string DbHost
        {
            get
            {
                if (DbHostProd == null)
                {
                    return DbHostDev;
                }
                else
                {
                    return DbHostProd;
                }
            }
        }

        private static string RabbitHostDev = "127.0.0.1";
        private static string RabbitHostProd = Environment.GetEnvironmentVariable("RABBIT_HOST");

        public static string RabbitHost
        {
            get
            {
                if (RabbitHostProd == null)
                {
                    return RabbitHostDev;
                }
                else
                {
                    return RabbitHostProd;
                }
            }
        }

        private static WorkMode GetMode()
        {
            string mode = Environment.GetEnvironmentVariable("MODE");
            if (mode == null)
            {
                return WorkMode.LOCAL;
            }
            return (WorkMode)Enum.Parse(typeof(WorkMode), mode);
        }
    }
}
