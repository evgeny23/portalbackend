﻿using System;
using System.ServiceModel;

namespace PortalBackend.Controllers.UserProductController
{
    public static class KeyCheckWcf
    {
        private static WCFServiceClient CreateClient()
        {
            var binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            var uri = "http://130.193.35.44:9011/UserDeterminerService";
            var endpoint = new EndpointAddress(new Uri(uri));
            return new WCFServiceClient(binding, endpoint);
        }

        public static string KeyChec(string serialKey)
        {
            var client = CreateClient();
            string productKey;
            bool result = client.GetProductKeyFromSerialKey(out productKey, serialKey);
            if (result)
            {
                return productKey;
            } else
            {
                return null;
            }
        }
    }
}
