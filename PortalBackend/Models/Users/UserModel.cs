﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalBackend.Models
{
    public class User: BaseUser
    {
        [MaxLength(254)]
        [MinLength(5)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Column(TypeName = "citext")]
        public string Email { get; set; }
        
        [JsonIgnore]
        [MaxLength(44)]
        [MinLength(22)]
        public string PasswordHash { get; set; }

        [JsonIgnore]
        [MaxLength(24)]
        [MinLength(12)]
        public string Salt { get; set; }

        [MaxLength(15)]
        [MinLength(8)]
        public string PhoneNumber { get; set; }

        public bool NumberValidate { get; set; }

        public bool EmailValidate { get; set; }

        public override string UserName
        {
            get
            {
                return Email;
            }
        }
    }
}
