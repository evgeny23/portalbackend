﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalBackend.Migrations
{
    public partial class gartens_and_parents02 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "role_permissions",
                keyColumns: new[] { "Permission", "RoleId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "role_permissions",
                keyColumns: new[] { "Permission", "RoleId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "role_permissions",
                keyColumns: new[] { "Permission", "RoleId" },
                keyValues: new object[] { 4, 2 });

            migrationBuilder.AddColumn<bool>(
                name: "DefaultAccount",
                table: "users",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ParentFName",
                table: "users",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ParentSName",
                table: "users",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true);

            migrationBuilder.InsertData(
                table: "roles",
                columns: new[] { "RoleId", "Custom", "Name" },
                values: new object[] { 4, false, "Родитель" });

            migrationBuilder.InsertData(
                table: "role_permissions",
                columns: new[] { "Permission", "RoleId" },
                values: new object[,]
                {
                    { 1, 4 },
                    { 2, 4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "role_permissions",
                keyColumns: new[] { "Permission", "RoleId" },
                keyValues: new object[] { 1, 4 });

            migrationBuilder.DeleteData(
                table: "role_permissions",
                keyColumns: new[] { "Permission", "RoleId" },
                keyValues: new object[] { 2, 4 });

            migrationBuilder.DeleteData(
                table: "roles",
                keyColumn: "RoleId",
                keyValue: 4);

            migrationBuilder.DropColumn(
                name: "DefaultAccount",
                table: "users");

            migrationBuilder.DropColumn(
                name: "ParentFName",
                table: "users");

            migrationBuilder.DropColumn(
                name: "ParentSName",
                table: "users");

            migrationBuilder.InsertData(
                table: "role_permissions",
                columns: new[] { "Permission", "RoleId" },
                values: new object[,]
                {
                    { 1, 2 },
                    { 2, 2 },
                    { 4, 2 }
                });
        }
    }
}
