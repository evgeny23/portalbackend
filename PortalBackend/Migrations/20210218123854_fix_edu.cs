﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalBackend.Migrations
{
    public partial class fix_edu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_product_education_products_ProductId1",
                table: "product_education");

            migrationBuilder.DropIndex(
                name: "IX_product_education_ProductId1",
                table: "product_education");

            migrationBuilder.DropColumn(
                name: "ProductId1",
                table: "product_education");

            migrationBuilder.AlterColumn<string>(
                name: "ProductId",
                table: "product_education",
                type: "citext",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.CreateIndex(
                name: "IX_product_education_ProductId",
                table: "product_education",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_product_education_products_ProductId",
                table: "product_education",
                column: "ProductId",
                principalTable: "products",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_product_education_products_ProductId",
                table: "product_education");

            migrationBuilder.DropIndex(
                name: "IX_product_education_ProductId",
                table: "product_education");

            migrationBuilder.AlterColumn<int>(
                name: "ProductId",
                table: "product_education",
                type: "integer",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "citext");

            migrationBuilder.AddColumn<string>(
                name: "ProductId1",
                table: "product_education",
                type: "citext",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_product_education_ProductId1",
                table: "product_education",
                column: "ProductId1");

            migrationBuilder.AddForeignKey(
                name: "FK_product_education_products_ProductId1",
                table: "product_education",
                column: "ProductId1",
                principalTable: "products",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
