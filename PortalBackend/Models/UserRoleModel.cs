﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PortalBackend.Models
{
    [Table("user_roles")]
    public class UserRole
    {
        public int UserId { get; set; }
        public BaseUser User { get; set; }

        public int RoleId { get; set; }
        public Role Role { get; set; }
    }
}
