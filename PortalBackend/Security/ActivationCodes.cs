﻿using Newtonsoft.Json;
using PortalBackend.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace PortalBackend.Security
{
    public enum ActivationType
    {
        Email = 0,
        Phone = 1,
        PasswordReset = 3,
    }

    public static class ActivationCodes
    {
        private static Random random = new Random();

        private static string GenerateKey(int length)
        {
            const string pool = "abcdefghijklmnopqrstuvwxyz0123456789";
            var builder = new StringBuilder();

            for (var i = 0; i < length; i++)
            {
                var c = pool[random.Next(0, pool.Length)];
                builder.Append(c);
            }
            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// Генерация пары Uid:Key
        /// </summary>
        /// <param name="codeLifetime">Время жизни кода активации</param>
        /// <param name="userId">Ид пользователя</param>
        /// <param name="at">Тип кода активации</param>
        /// <param name="lockTimeout">Время, через которое для данного типа кода активации снова можно будет генерировать новую ссылку</param>
        /// <returns>(UID, Key)</returns>
        public static (string, string) Create(int codeLifetime, int userId, ActivationType at, int lockTimeout)
        {
            string lockKey = at.ToString() + "_" + userId.ToString();
            if (Redis.GetData(lockKey, (int)RedisDatabases.ActivationCodes) != null)
                return (null, null);
            string id = Guid.NewGuid().ToString();
            string key = GenerateKey(6);
            string data = JsonConvert.SerializeObject(new Dictionary<string, string>()
            {
                {"key", key},
                {"userId",  userId.ToString()},
            });
            Redis.AddData(id, data, (int)RedisDatabases.ActivationCodes, codeLifetime);
            Redis.AddData(lockKey, "", (int)RedisDatabases.ActivationCodes, lockTimeout);
            return (id, key);
        }

        /// <summary>
        /// Метод для проверки кода активации.
        /// </summary>
        /// <param name="cid">CodeId. Идентификатор кода активации.</param>
        /// <param name="secretKey">Непосредственно секретный код</param>
        /// <returns>int: userId; Идентификатор пользователя, для которого был создан код</returns>
        public static int Check(string cid, string secretKey)
        {
            string value = Redis.GetData(cid, (int)RedisDatabases.ActivationCodes);
            Dictionary<string, string> data;
            try
            {
                data = JsonConvert.DeserializeObject<Dictionary<string, string>>(value);
            }
            catch
            {
                return -1;
            }
            if (data["key"] == secretKey)
            {
                Redis.RemoveData(cid, (int)RedisDatabases.ActivationCodes);
                return Convert.ToInt32(data["userId"]);
            }
            return -1;
        }
    }
}
