﻿using PortalBackend.Models;
using PortalBackend.Responses;
using PortalBackend.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBackend.Controllers.Users.PedagogsController
{
    public class PedagogsDbMethods
    {
        internal static BaseResponse CreateNewPedagog(PedagogData data, int parentUserId)
        {
            ApplicationContext db = new ApplicationContext();
            Pedagog pedagog = new Pedagog()
            {
                ParentUserId = parentUserId,
                FIO = data.FIO,
                Country = data.Country,
                State = data.State,
                City = data.City,
                Organization = data.Organization,
                Position = data.Position,
                RegistrationDate = DateTime.Now,
                DefaultAccount = false,
            };
            db.Pedagogs.Add(pedagog);
            db.SaveChanges();
            DefaultRoles.SetUserRole(pedagog.UserId, DefaultRolesEnum.Pedagog);
            return StandartResponses.Ok("Успешно", pedagog);
        }

        internal static BaseResponse UpdatePedagog(PedagogData data, int userId)
        {
            ApplicationContext db = new ApplicationContext();
            Pedagog pedagog = db.Pedagogs.Where(p => p.UserId == userId).FirstOrDefault();
            pedagog.FIO = data.FIO;
            pedagog.Country = data.Country;
            pedagog.City = data.City;
            pedagog.State = data.State;
            pedagog.Organization = data.Organization;
            pedagog.Position = data.Position;
            db.Pedagogs.Update(pedagog);
            db.SaveChanges();
            return StandartResponses.Ok("Успешно", pedagog);
        }
    }
}
