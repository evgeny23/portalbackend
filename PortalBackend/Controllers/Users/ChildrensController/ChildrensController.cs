﻿using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Models;
using PortalBackend.Responses;
using PortalBackend.Security;
using PortalBackend.Services;

namespace PortalBackend.Controllers.Users.ChildrensController
{
    [Route("api/childrens")]
    [ApiController]
    public class ChildrensController
    {
        /// <summary>
        /// Регистрация ребёнка. Доступно только родителям
        /// [CheckPermissions(PermissionsEnum.ChildrensControll, "parentId")]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса. </response>
        /// <response code="401">Не авторизован</response>
        /// <response code="403">Доступ к ресурсу запрещён</response>
        [HttpPost("/create/{parentId}")]
        [CheckPermissions(PermissionsEnum.ChildrensControll, "parentId")]
        public IActionResult CreateChildren(int parentId, [FromBody] ChildrenCreateData data)
        {
            Parent user = CommonDbMethods.GetUserById(parentId) as Parent;
            if (user == null)
                return StandartResponses.BadRequest("Доступно только родителям").Resp();
            return ChildrensDbMethods.CreateChildren(parentId, data).Resp();
        }

        /// <summary>
        /// Получение аккаунта ребёнка
        /// [CheckPermissions(PermissionsEnum.ChildrensControll, "parentId")]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса. </response>
        /// <response code="401">Не авторизован</response>
        /// <response code="403">Доступ к ресурсу запрещён</response>
        [HttpGet("/get_account/{childrenId}")]
        [CheckPermissions(PermissionsEnum.Common, "childrenId")]
        public IActionResult CreateChildren(int childrenId)
        {
            return ChildrensDbMethods.GetChildrenData(childrenId).Resp();
        }
    }
}
