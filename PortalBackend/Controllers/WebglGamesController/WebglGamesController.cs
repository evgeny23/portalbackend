﻿using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Responses;
using PortalBackend.Security;

namespace PortalBackend.Controllers.WebglGamesController
{
    public class AddGameData
    {
        public string name { get; set; }
        public string path { get; set; }
    }

    [Route("api/webgl_games")]
    [ApiController]
    public class WebglGamesController : ControllerBase
    {
        /// <summary>
        /// Добавление новой Webgl игры
        /// [CheckPermissions(PermissionsEnum.Admin)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса</response>
        [HttpPost]
        [CheckPermissions(PermissionsEnum.Admin)]
        public IActionResult Post([FromBody] AddGameData data)
        {
            return DbMethods.CreateNewGame(data).Resp();
        }

        /// <summary>
        /// Получение списка webgl игр с пагинацией
        /// [CheckPermissions(PermissionsEnum.Common)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса. </response>
        /// <response code="401">Не авторизован</response>
        /// <response code="403">Доступ к ресурсу запрещён</response>
        [HttpGet("list/{offset}/{count}")]
        [CheckPermissions(PermissionsEnum.Common)]
        public IActionResult GetListOfGames(int offset, int count)
        {
            if (offset < 0 || count < 1 || count > Config.maxPaginationCountLimit)
            {
                return new BaseResponse()
                {
                    ResponseType = ResponseType.Error,
                    message = "Неверные offset/count",
                    statusCode = 400,
                }.Resp();
            }
            return DbMethods.GetListOfGames(offset, count).Resp();
        }

        /// <summary>
        /// Получение конкретной игры по ID
        /// [CheckPermissions(PermissionsEnum.Common)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса. </response>
        /// <response code="401">Не авторизован</response>
        /// <response code="403">Доступ к ресурсу запрещён</response>
        [HttpGet("webglgame/{gameId}")]
        [CheckPermissions(PermissionsEnum.Common)]
        public IActionResult GetListWithPagination(int gameId)
        {
            return DbMethods.GetWebglGameById(gameId).Resp();
        }
    }
}
