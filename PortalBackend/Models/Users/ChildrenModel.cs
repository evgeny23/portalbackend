﻿using System.ComponentModel.DataAnnotations;

namespace PortalBackend.Models
{
    public class Children: BaseUser
    {
        [Required]
        public string FIO { get; set; }

        public override string UserName
        {
            get
            {
                return FIO;
            }
        }
    }
}
