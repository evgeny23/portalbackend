﻿using PortalBackend.Models;
using PortalBackend.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBackend.Controllers.Users.ChildrensController
{
    internal static class ChildrensDbMethods
    {
        internal static BaseResponse CreateChildren(int parentId, ChildrenCreateData data)
        {
            if (data.FIO.Length < 3)
                return StandartResponses.BadRequest("Слишком короткое ФИО");
            Children children = new Children()
            {
                FIO = data.FIO
            };
            ApplicationContext db = new ApplicationContext();
            db.Childrens.Add(children);
            db.ChildrenParents.Add(
                new ChildrenParent()
                {
                    ChildrenId = children.UserId,
                    ParentId = parentId,
                }
            );
            db.SaveChanges();
            return StandartResponses.Ok("Ребёнок создан", children);
        }

        internal static BaseResponse GetChildrenData(int userId)
        {
            ApplicationContext db = new ApplicationContext();
            Children children = db.Childrens.Where(c => c.UserId == userId).FirstOrDefault();
            if (children == null)
                return StandartResponses.BadRequest("Ребёнка не существует", children.UserId);
            return StandartResponses.Ok("Аккаунт ребёнка", children);
        }
    }
}
