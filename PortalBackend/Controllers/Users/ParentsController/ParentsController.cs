﻿using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Responses;
using PortalBackend.Security;
using PortalBackend.Services;
using System;

namespace PortalBackend.Controllers.ParentsController
{
    [Route("api/parents")]
    [ApiController]
    public class ParentsController : ControllerBase
    {
        /// <summary>
        /// Регистрация родителя
        /// [CheckPermissions(PermissionsEnum.Common, "", true)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса</response>
        [HttpPost]
        [CheckPermissions(PermissionsEnum.Common, "", true)]
        public IActionResult Post([FromBody] ParentRegisterData data)
        {
            int userId = CommonDbMethods.GetMasterUserId(Convert.ToInt32(JwtTokens.GetValueFromPayload(Request.Headers, "userId")));
            if (userId == -1)
                return StandartResponses.BadRequest("Ошибка при создании аккаунта").Resp();
            return ParentsDbMethods.CreateNewParent(data, userId).Resp();
        }

        /// <summary>
        /// Обновление информации о родителе
        /// [CheckPermissions(PermissionsEnum.Common, "userId", true)]
        /// </summary>
        /// <param name="data">ParentRegisterData</param>
        /// <param name="userId">ид родителя</param>
        /// <returns></returns>
        [HttpPut("parent_update/{userId}")]
        [CheckPermissions(PermissionsEnum.Common, "userId", true)]
        public IActionResult UpdateParent([FromBody] ParentRegisterData data,  int userId)
        {
            return ParentsDbMethods.UpdateParent(data, userId).Resp();
        }
    }
}