﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PortalBackend.Models
{
    [Table("regions")]
    public class Region
    {
        public string Country { get; set; }
        
        public string State { get; set; }

        public string City { get; set; }
    }
}
