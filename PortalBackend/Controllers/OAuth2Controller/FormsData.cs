﻿namespace PortalBackend.Controllers.OAuth2Controller
{
    public class AuthenticationData
    {
        public string email { get; set; }
        public string password { get; set; }
    }

    public class RefreshData
    {
        public string token { get; set; }
    }
}
