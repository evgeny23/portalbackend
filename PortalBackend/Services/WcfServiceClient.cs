﻿namespace UserDeterminer
{
    using System.Runtime.Serialization;

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "UserData", Namespace = "http://schemas.datacontract.org/2004/07/UserDeterminer")]
    public partial class UserData : object, System.Runtime.Serialization.IExtensibleDataObject
    {
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        private string ProductNameField;

        private string ShortKeyField;

        private string UserNameField;

        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get { return this.extensionDataField; }
            set { this.extensionDataField = value; }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProductName
        {
            get { return this.ProductNameField; }
            set { this.ProductNameField = value; }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ShortKey
        {
            get { return this.ShortKeyField; }
            set { this.ShortKeyField = value; }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserName
        {
            get { return this.UserNameField; }
            set { this.UserNameField = value; }
        }
    }
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
[System.ServiceModel.ServiceContractAttribute(ConfigurationName = "IWCFService")]
public interface IWCFService
{
    [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IWCFService/GetJsonUsers", ReplyAction = "http://tempuri.org/IWCFService/GetJsonUsersResponse")]
    string[] GetJsonUsers(string productName, string userSerial);

    [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IWCFService/GetUsers", ReplyAction = "http://tempuri.org/IWCFService/GetUsersResponse")]
    UserDeterminer.UserData[] GetUsers(string productName, string userSerial);

    [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IWCFService/GetProductKeyFromSerialKey", ReplyAction = "http://tempuri.org/IWCFService/GetProductKeyFromSerialKeyResponse")]
    bool GetProductKeyFromSerialKey(out string productKey, string serialKey);

    [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IWCFService/Test", ReplyAction = "http://tempuri.org/IWCFService/TestResponse")]
    bool Test();
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
public interface IWCFServiceChannel : IWCFService, System.ServiceModel.IClientChannel { }

[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
public partial class WCFServiceClient : System.ServiceModel.ClientBase<IWCFService>, IWCFService
{
    public WCFServiceClient() { }

    public WCFServiceClient(string endpointConfigurationName) :
        base(endpointConfigurationName)
    { }

    public WCFServiceClient(string endpointConfigurationName, string remoteAddress) :
        base(endpointConfigurationName, remoteAddress)
    { }

    public WCFServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
        base(endpointConfigurationName, remoteAddress)
    { }

    public WCFServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
        base(binding, remoteAddress)
    { }

    public string[] GetJsonUsers(string productName, string userSerial)
    {
        return base.Channel.GetJsonUsers(productName, userSerial);
    }

    public UserDeterminer.UserData[] GetUsers(string productName, string userSerial)
    {
        return base.Channel.GetUsers(productName, userSerial);
    }

    public bool GetProductKeyFromSerialKey(out string productKey, string serialKey)
    {
        return base.Channel.GetProductKeyFromSerialKey(out productKey, serialKey);
    }

    public bool Test()
    {
        return base.Channel.Test();
    }
}