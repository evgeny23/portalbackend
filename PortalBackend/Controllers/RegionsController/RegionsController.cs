﻿using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Security;

namespace PortalBackend.Controllers.RegionsController
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegionsController
    {
        /// <summary>
        /// Добавление локации
        /// [CheckPermissions(PermissionsEnum.Admin)]
        /// </summary>
        /// <param name="data">LocationData</param>
        /// <returns></returns>
        [HttpPost]
        [CheckPermissions(PermissionsEnum.Admin)]
        public IActionResult AddNewLocation([FromBody] LocationData data)
        {
            return RegionsDbMethods.AddNewLocation(data).Resp();
        }

        /// <summary>
        /// Получение всех городов
        /// </summary>
        /// <returns></returns>
        [HttpGet("get_all_data")]
        public IActionResult GetAllData()
        {
            return RegionsDbMethods.GetAllData().Resp();
        }

        /// <summary>
        /// Получение доступных стран
        /// </summary>
        /// <returns></returns>
        [HttpGet("get_countries")]
        public IActionResult GetCountries()
        {
            return RegionsDbMethods.GetCountries().Resp();
        }

        /// <summary>
        /// Получение регионов для страны
        /// </summary>
        /// <param name="country">Страна</param>
        /// <returns></returns>
        [HttpGet("get_states/{country}")]
        public IActionResult GetStates(string country)
        {
            return RegionsDbMethods.GetStates(country).Resp();
        }

        /// <summary>
        /// Получение городов для страны и региона
        /// </summary>
        /// <param name="country">Страна</param>
        /// <param name="state">Регион</param>
        /// <returns></returns>
        [HttpGet("get_cities/{country}/{state}")]
        public IActionResult GetCities(string country, string state)
        {
            return RegionsDbMethods.GetCities(country, state).Resp();
        }
    }
}
