﻿using JWT.Algorithms;
using JWT.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PortalBackend.Models;
using System;
using System.Collections.Generic;

namespace PortalBackend.Security
{
    public static class JwtTokens
    {
        /// <summary>
        /// Метод для извлечения значений из payload JWT-токена, с приведением к нужному типу
        /// </summary>
        /// <typeparam name="T">Возвращаемый тип из payload</typeparam>
        /// <param name="headers">Заголовки запроса, для получения токена</param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object GetValueFromPayload(IHeaderDictionary headers, string key)
        {
            StringValues jwtToken;
            headers.TryGetValue("Authorization", out jwtToken);
            var payload = GetPayloadFromToken(jwtToken);
            return payload[key];
        }

        /// <summary>
        /// Вернет все Permission данного токена, если он валидный, либо вызовет exception
        /// </summary>
        public static List<PermissionsEnum> VerifyAndGetPermissionsList(string token)
        {
            var payload = GetPayloadFromToken(token);
            JArray ar = payload["perms"] as JArray;
            return ar.ToObject<List<PermissionsEnum>>();
        }

        /// <summary>
        /// Вернет Dictionary токенов с ключами refresh и access
        /// </summary>
        public static Dictionary<string, string> GeneratePairTokens(User user, List<PermissionsEnum> perms, BaseUser childUser = null)
        {
            string accessToken = CreateAccessToken(user, perms, childUser);
            string refreshToken = CreateRefreshToken();
            return new Dictionary<string, string>
            {
                { "refresh", refreshToken },
                { "access", accessToken },
            };
        }
        
        private static IDictionary<string, object> GetPayloadFromToken(string token)
        {
            return  new JwtBuilder()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .WithSecret(SecurityConfig.KEY)
                .MustVerifySignature()
                .Decode<IDictionary<string, object>>(token.Split()[1]);
        }

        private static string CreateAccessToken(User user, List<PermissionsEnum> perms, BaseUser childUser = null)
        {
            var payload = new Dictionary<string, object>
            {
                { "user_name", user.UserName },
                { "userId", childUser != null ? childUser.UserId : user.UserId},
                { "email_active", user.EmailValidate },
                { "phone_active", user.NumberValidate },
                { "email", user.Email },
                { "defaultAccount", childUser != null ? childUser.DefaultAccount : false },
                { "phone", user.PhoneNumber },
                { "perms", perms },
                { "avatar",  user.Avatar },
                { "userType", childUser != null ? childUser.GetType().Name : user.GetType().Name },
                { "userData", childUser != null ? JsonConvert.SerializeObject(childUser) : "" },
            };
            return CreateJwtToken(SecurityConfig.ACCES_TOKEN_LIFETIME, payload);
        }

        private static string CreateRefreshToken()
        {
            return Guid.NewGuid().ToString();
        }

        private static string CreateJwtToken(int lifetime, Dictionary<string, object> payload = null)
        {
            if (payload == null)
                payload = new Dictionary<string, object>();
            return new JwtBuilder()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .WithSecret(SecurityConfig.KEY)
                .AddClaims(payload)
                .AddClaim("exp", DateTimeOffset.UtcNow.AddSeconds(lifetime).ToUnixTimeSeconds())
                .Encode();
        }
    }
}
