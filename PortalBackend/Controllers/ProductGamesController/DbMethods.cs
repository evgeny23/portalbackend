﻿using PortalBackend.Models;
using PortalBackend.Responses;
using System.Collections.Generic;
using System.Linq;

namespace PortalBackend.Controllers.ProductGamesController
{
    public static class DbMethods
    {
        public static BaseResponse AddNewGameToProduct(GameData data)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                if (db.Games.Where( g => g.GameName == data.name && g.ProductId == data.productKey ).FirstOrDefault() != null)
                {
                    return StandartResponses.BadRequest("Такая игра не существует");
                }
                if (db.Products.Where(p => p.ProductId == data.productKey).FirstOrDefault() == null)
                {
                    return StandartResponses.BadRequest("Такого продукта не существует");
                }
                Game game = new Game()
                {
                    GameName = data.name,
                    ProductId = data.productKey,
                };
                db.Add(game);
                db.SaveChanges();
                return StandartResponses.Ok("Игра успешно добавлена", game);
            }
        }

        public static BaseResponse GetGameById(string productKey, string serialHash, int gameId, int userId)
        {
            if (!CheckSerialHashAndUserId(userId, serialHash))
                return StandartResponses.Unauthorized("Данный продукт не принадлежит пользователю");
            Game game;
            using (ApplicationContext db = new ApplicationContext())
            {
                game = db.Games.Where(g => g.GameId == gameId && g.ProductId == productKey).FirstOrDefault();
            }
            if (game == null)
            {
                return StandartResponses.BadRequest("Игры с таким id не существует");
            }
            game.IsRun = game.CheckIsRun(serialHash);
            return StandartResponses.Ok("Игра с заданным id", game);
        }

        public static BaseResponse GetGamesWithPagination(string productKey, string serialHash, int userId, int offset, int count)
        {
            if (!CheckSerialHashAndUserId(userId, serialHash))
                return StandartResponses.Unauthorized("Данный продукт не принадлежит пользователю");
            List<Game> games;
            using (ApplicationContext db = new ApplicationContext())
            {
                games = db.Games
                    .Where(g => g.ProductId == productKey)
                    .OrderBy(g => g.GameId)
                    .Skip(offset)
                    .Take(count)
                    .ToList();
                games = games
                    .Select(g => { g.IsRun = g.CheckIsRun(serialHash); return g; })
                    .ToList();
            }
            return StandartResponses.Ok("Список игр", games);
        }

        public static BaseResponse AddTagToGame(string productKey, int gameId, string tagName)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                if (db.Games.Where( g => g.GameId == gameId && g.ProductId == productKey).Count() == 0)
                {
                    return StandartResponses.BadRequest("Такой игры не существует");
                }
                if (db.GameTags.Where( gt => gt.GameId == gameId && gt.ProductId == productKey && gt.Tag == tagName ).FirstOrDefault() != null)
                {
                    return StandartResponses.BadRequest("Данный тег у игры уже имеется");
                }
                GameTag tag = new GameTag()
                {
                    GameId = gameId,
                    ProductId = productKey,
                    Tag = tagName,
                };
                db.GameTags.Add(tag);
                db.SaveChanges();
                return StandartResponses.Ok("Тег успешно добавлен", tag);
            }
        }

        public static BaseResponse GetTagsWithPagination(string productKey, int gameId, int offset, int count)
        {
            List<GameTag> tags;
            using (ApplicationContext db = new ApplicationContext())
            {
                tags = db.GameTags
                    .Where( gt => gt.GameId == gameId && gt.ProductId == productKey)
                    .OrderBy(gt => gt.GameId)
                    .Skip(offset)
                    .Take(count)
                    .ToList();
            }
            return StandartResponses.Ok("Список тегов", tags);
        }

        public static BaseResponse GetGamesByTags(List<string> tags)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                List<Game> games = db.Games.Join(
                    db.GameTags,
                    game => game.GameId,
                    tag => tag.GameId,
                    (game, tag) =>
                    new {
                        game = game,
                        tag = tag,
                    })
                    .Where(r => tags.Contains(r.tag.Tag.ToLower()))
                    .Select(r => r.game).Distinct()
                    .ToList();
                return StandartResponses.Ok("Список игр, относящихся к данным тегам", games);
            }
        }

        private static bool CheckSerialHashAndUserId(int userId, string serialHash)
        {
            ApplicationContext db = new ApplicationContext();
            UserProduct product = db.UserProducts.Where(up => up.SerialHash == serialHash && up.UserId == userId).FirstOrDefault();
            if (product != null)
                return true;
            return false;
        }
    }
}
