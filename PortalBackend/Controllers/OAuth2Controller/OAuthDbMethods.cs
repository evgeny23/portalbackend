﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PortalBackend.Models;
using PortalBackend.Responses;
using PortalBackend.Security;
using PortalBackend.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PortalBackend.Controllers.OAuth2Controller
{
    public class RedisUserData
    {
        public int userId { get; set; }
        public int parentId { get; set; }
    }

    internal static class OAuthDbMethods
    {
        internal static BaseResponse Authorize(AuthenticationData data)
        {
            ApplicationContext db = new ApplicationContext();
            User user = db.Users.Where(u => u.Email == data.email).FirstOrDefault();
            if (user == null)
                return StandartResponses.Unauthorized("Неверные логин или пароль");

            Dictionary<string, string> result = PasswordHashing.GetPasswordHashAndSalt(data.password, user.Salt);
            if (result["hash"] != user.PasswordHash)
                return StandartResponses.Forbidden("Неверные логин или пароль");

            List<BaseUser> accs = db.AllUsers.Where(au => au.ParentUserId == user.UserId).ToList();
            BaseUser baseUser = accs.OrderByDescending( a => a.DefaultAccount ).FirstOrDefault();
            List<PermissionsEnum> perms;
            RedisUserData redisData;
            if (baseUser == null)
            {
                perms = GetPermissions(user.UserId);
                redisData = new RedisUserData() { userId = user.UserId, parentId = user.ParentUserId };
            }
            else
            {
                perms = GetPermissions(baseUser.UserId);
                redisData = new RedisUserData() { userId = baseUser.UserId, parentId = baseUser.ParentUserId };
            }
            Dictionary<string, string> tokens = JwtTokens.GeneratePairTokens(user, perms, baseUser);
            
            Redis.AddData(
                tokens["refresh"], 
                JsonConvert.SerializeObject(redisData), 
                (int)RedisDatabases.OAuth, 
                SecurityConfig.REFRESH_TOKEN_LIFETIME
            );
            return StandartResponses.Ok("Успешно", tokens);
        }

        internal static BaseResponse Refresh(RefreshData data)
        {
            if (data.token != null && data.token.Length < 10)
            {
                return StandartResponses.BadRequest("Некорректный токен");
            }
            string redisData = Redis.GetData(data.token, (int)RedisDatabases.OAuth);
            if (redisData == null)
            {
                return StandartResponses.BadRequest("Некорректный токен");
            }
            RedisUserData userData = JsonConvert.DeserializeObject<RedisUserData>(redisData);
            Redis.RemoveData(data.token, (int)RedisDatabases.OAuth);
            ApplicationContext db = new ApplicationContext();
            if (userData.parentId == 0)
            {
                User user = db.Users.Where(u => u.UserId == userData.userId).FirstOrDefault();
                List<PermissionsEnum> perms = GetPermissions(userData.userId);
                Dictionary<string, string> tokens = JwtTokens.GeneratePairTokens(user, perms);
                RedisUserData newRedisData = new RedisUserData() { userId = user.UserId, parentId = user.ParentUserId };
                Redis.AddData(
                    tokens["refresh"],
                    JsonConvert.SerializeObject(newRedisData),
                    (int)RedisDatabases.OAuth,
                    SecurityConfig.REFRESH_TOKEN_LIFETIME
                );
                return StandartResponses.Ok("Успешно", tokens);
            } 
            else
            {
                User user = db.Users.Where(u => u.UserId == userData.parentId).FirstOrDefault();
                BaseUser baseUser = db.AllUsers.Where(bu => bu.UserId == userData.userId).FirstOrDefault();
                List<PermissionsEnum> perms = GetPermissions(userData.userId);
                Dictionary<string, string> tokens = JwtTokens.GeneratePairTokens(user, perms, baseUser);
                RedisUserData newRedisData = new RedisUserData() { userId = baseUser.UserId, parentId = baseUser.ParentUserId };
                Redis.AddData(
                    tokens["refresh"],
                    JsonConvert.SerializeObject(newRedisData),
                    (int)RedisDatabases.OAuth,
                    SecurityConfig.REFRESH_TOKEN_LIFETIME
                );
                return StandartResponses.Ok("Успешно", tokens);
            }
        }

        internal static BaseResponse ChangeRelatedAccount(int userId, int relatedUserId)
        {
            ApplicationContext db = new ApplicationContext();
            BaseUser currentUser = db.AllUsers.Where(bu => bu.UserId == userId).FirstOrDefault();
            if (currentUser == null || currentUser.ParentUserId == 0)
                return StandartResponses.BadRequest("Ошибка при получении пользователя");
            User masterUser = db.Users.Where(u => u.UserId == currentUser.ParentUserId).FirstOrDefault();
            BaseUser targetUser = db.AllUsers.Where(bu => bu.ParentUserId == currentUser.ParentUserId && bu.UserId == relatedUserId).FirstOrDefault();
            List<PermissionsEnum> perms = GetPermissions(targetUser.UserId);
            Dictionary<string, string> tokens = JwtTokens.GeneratePairTokens(masterUser, perms, targetUser);
            RedisUserData newRedisData = new RedisUserData() { userId = targetUser.UserId, parentId = targetUser.ParentUserId };
            Redis.AddData(
                tokens["refresh"],
                JsonConvert.SerializeObject(newRedisData),
                (int)RedisDatabases.OAuth,
                SecurityConfig.REFRESH_TOKEN_LIFETIME
            );
            return StandartResponses.Ok("Успешно", tokens);
        }

        internal static List<PermissionsEnum> GetPermissions(int userId)
        {
            ApplicationContext db = new ApplicationContext();
            var query = db.AllUsers.Where( au => au.UserId == userId).Join(
                db.UserRoles,
                user => user.UserId,
                role => role.User.UserId,
                (user, role) => new
                {
                    User = user,
                    Role = role,
                }
            ).Join(
                db.RolePermissions,
                role => role.Role.RoleId,
                rp => rp.Role.RoleId,
                (role, rp) => new
                {
                    user = role.User,
                    perm = rp.Permission,
                }
            );
            var perm = query.Where(p => p.user.UserId == userId).ToList();
            List<PermissionsEnum> perms = perm.Select(p => p.perm).ToList();
            return perms;
        }

        internal static List<BaseUser> GetChildAccounts(int userId)
        {
            ApplicationContext db = new ApplicationContext();
            return db.AllUsers.Where(au => au.ParentUserId == userId).ToList();
        }
    }
}
