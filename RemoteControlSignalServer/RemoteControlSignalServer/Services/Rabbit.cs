﻿using RabbitMQ.Client;

namespace RemoteControlSignalServer
{
    public class Rabbit
    {
        public IConnection conn;
        public IModel channel;

        public Rabbit()
        {
            ConnectionFactory factory = new ConnectionFactory();
            factory.UserName = "rabbit";
            factory.Password = "rabbit";
            factory.VirtualHost = "/";
            factory.HostName = Config.RabbitHost;
            conn = factory.CreateConnection();
            channel = conn.CreateModel();
            channel.ExchangeDeclare("pc", ExchangeType.Direct);
            channel.QueueDeclare("que", false, false, false, null);
            channel.QueueDeclarePassive("que");
        }
    }

    public static class RabbitChannel
    {
        public static Rabbit rabbit;

        public static Rabbit GetRabbit()
        {
            if (rabbit != null && rabbit.conn.IsOpen)
            {
                return rabbit;
            }
            try
            {
                rabbit = new Rabbit();
                return rabbit;
            }
            catch
            {
                return null;
            }
        }
    }
}
