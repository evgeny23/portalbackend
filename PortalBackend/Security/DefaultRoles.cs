using PortalBackend.Models;
using System.Collections.Generic;

namespace PortalBackend.Security
{
    public enum DefaultRolesEnum
    {
        Admin = 1,
        Registered = 2,
        Garten = 3,
        Parent = 4,
        Pedagog = 5,
    }

    public static class DefaultRoles
    {
        public static void SetUserRole(int userId, DefaultRolesEnum role)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.UserRoles.Add(
                    new UserRole
                    {
                        UserId = userId,
                        RoleId = (int)role,
                    }
                );
                db.SaveChanges();
            }
        }

        public static List<Role> roles = new List<Role>() {
            new Role { Name = "Администратор", Custom = false, RoleId = (int)DefaultRolesEnum.Admin },
            new Role { Name = "Зарегистрированный пользователь", Custom = false, RoleId = (int)DefaultRolesEnum.Registered },
            new Role { Name = "Детский сад", Custom = false, RoleId = (int)DefaultRolesEnum.Garten },
            new Role { Name = "Родитель", Custom = false, RoleId = (int)DefaultRolesEnum.Parent },
            new Role { Name = "Педагог", Custom = false, RoleId = (int)DefaultRolesEnum.Pedagog },
        };

        public static List<RolePermission> permissions = new List<RolePermission>() {
            new RolePermission { Permission = PermissionsEnum.Admin, RoleId = (int)DefaultRolesEnum.Admin },
            new RolePermission { Permission = PermissionsEnum.Common, RoleId = (int)DefaultRolesEnum.Registered },
            new RolePermission { Permission = PermissionsEnum.Common, RoleId = (int)DefaultRolesEnum.Parent },
            new RolePermission { Permission = PermissionsEnum.News, RoleId = (int)DefaultRolesEnum.Parent },
            new RolePermission { Permission = PermissionsEnum.Common, RoleId = (int)DefaultRolesEnum.Garten },
            new RolePermission { Permission = PermissionsEnum.News, RoleId = (int)DefaultRolesEnum.Garten },
            new RolePermission { Permission = PermissionsEnum.Products, RoleId = (int)DefaultRolesEnum.Garten },
            new RolePermission { Permission = PermissionsEnum.Common, RoleId = (int)DefaultRolesEnum.Pedagog },
            new RolePermission { Permission = PermissionsEnum.News, RoleId = (int)DefaultRolesEnum.Pedagog },
        };
    }
}
