﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Models;
using PortalBackend.Responses;
using PortalBackend.Security;
using PortalBackend.Security.FileValidators;
using PortalBackend.Services;
using System;
using System.IO;
using static PortalBackend.Security.FileValidators.ImageValidator;

namespace PortalBackend.Controllers.Users.BaseUserController
{
    /// <summary>
    /// Базовые действия над пользователями, доступные для всех типов аккаунтов
    /// </summary>
    [Route("api/base_user")]
    [ApiController]
    public class BaseUserController
    {
        #region получение пользователей
        /// <summary>
        /// Получение существующего пользователя. Доступно только самому пользователю и админам
        /// [CheckPermissions(PermissionsEnum.Common, "id")]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса. </response>
        /// <response code="401">Не авторизован</response>
        /// <response code="403">Доступ к ресурсу запрещён</response>
        [HttpGet("{id}")]
        [CheckPermissions(PermissionsEnum.Common, "id")]
        public IActionResult Get(int id)
        {
            BaseUser user = CommonDbMethods.GetUserById(id);
            return StandartResponses.Ok("Пользователь", user).Resp();
        }

        /// <summary>
        /// Получение списка пользователей с пагинацией
        /// [CheckPermissions(PermissionsEnum.Common)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса. </response>
        /// <response code="401">Не авторизован</response>
        /// <response code="403">Доступ к ресурсу запрещён</response>
        [HttpGet("list/{offset}/{count}")]
        [CheckPermissions(PermissionsEnum.Common)]
        public IActionResult GetListWithPagination(int offset, int count)
        {
            if (offset < 0 || count < 1 || count > Config.maxPaginationCountLimit)
            {
                return StandartResponses.BadRequest("Неверные offset/count").Resp();
            }
            return BaseUserDbMethods.GetUsersWithPagination(offset, count).Resp();
        }
        #endregion

        #region связанные аккаунты
        /// <summary>
        /// Получение списка связанных аккаунтов (дет.сады, родительские аккаунты, педагогические, etc)
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">Идентификатор текущего пользователя</param>
        /// <returns></returns>
        [HttpGet("related/{userId}")]
        [CheckPermissions(PermissionsEnum.Common, "userId", true)]
        public IActionResult GetRelatedAccounts(int userId)
        {
            return BaseUserDbMethods.GetRelatedAccounts(userId).Resp();
        }

        /// <summary>
        /// Задать аккаунт дефолтным
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">Идентификатор пользователя, который должен быть по-умолчанию</param>
        /// <returns></returns>
        [HttpPut("set_default_account/{userId}")]
        [CheckPermissions(PermissionsEnum.Common, "", true)]
        public IActionResult SetDefaultAccount(int userId)
        {
            return BaseUserDbMethods.SetDefaultAccount(userId).Resp();
        }

        /// <summary>
        /// Раздефолтить аккаунт
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">Идентификатор пользователя, с которого надо снять дефолтность</param>
        /// <returns></returns>
        [HttpPut("set_undefault_account/{userId}")]
        [CheckPermissions(PermissionsEnum.Common, "", true)]
        public IActionResult SetAccountNotDefault(int userId)
        {
            return BaseUserDbMethods.SetAccountNotDefault(userId).Resp();
        }
        #endregion

        #region Управление аватарками
        /// <summary>
        /// Загрузка аватара для пользователя: JPG,BMP,GIF,PNG
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">Идентификатор текущего пользователя</param>
        /// <param name="avatar">Файл-аватарка</param>
        /// <returns></returns>
        [HttpPut("upload_avatar/{userId}")]
        [CheckPermissions(PermissionsEnum.Common, "userId")]
        public IActionResult UploadAvatar(int userId, IFormFile avatar)
        {
            if (avatar != null)
            {
                var memoryStream = new MemoryStream();
                avatar.CopyTo(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);
                ImageType imgType;
                if (!ImageValidator.IsImage(memoryStream, out imgType))
                    return StandartResponses.BadRequest("Некорректное изображение").Resp();
                memoryStream.Seek(0, SeekOrigin.Begin);
                string fileName = Guid.NewGuid().ToString() + "." + imgType.ToString();
                StaticServer.UploadFile("avatars", fileName, memoryStream);
                string link = "http://10.6.6.6:88/static/avatars/" + fileName;
                return BaseUserDbMethods.AddAvatarToUser(userId, link).Resp();
            }
            return StandartResponses.BadRequest("Ошибка при загрузке").Resp();
        }

        /// <summary>
        /// Удаление аватара
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">Идентификатор текущего пользователя</param>
        /// <returns></returns>
        [HttpDelete("delete_avatar/{userId}")]
        [CheckPermissions(PermissionsEnum.Common, "userId")]
        public IActionResult DeleteAvatar(int userId)
        {
            return BaseUserDbMethods.DeleteAvatar(userId).Resp();
        }
        #endregion
    }
}
