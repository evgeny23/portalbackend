﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace PortalBackend.Models
{
    [Table("users")]
    public abstract class BaseUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime RegistrationDate { get; set; }

        public int ParentUserId { get; set; }

        public bool DefaultAccount { get; set; }

        [JsonIgnore]
        public string AvatarLink { get; set; }
        
        [NotMapped]
        public string Avatar { 
            get 
            { 
                if (AvatarLink == null || AvatarLink.Length < 10)
                {
                    return Config.defaultAvatar;
                }
                return AvatarLink;
            }
        }

        [NotMapped]
        public string UserType => this.GetType().Name;

        public abstract string UserName { get; }
    }
}
