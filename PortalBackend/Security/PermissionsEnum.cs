﻿namespace PortalBackend.Security
{
    /// <summary>
    /// Менять местами значения в данном enum нельзя, т.к. интовое представление == идентификатор в базе данных
    /// </summary>
    public enum PermissionsEnum
    {
        Admin = 0,
        Common = 1,
        News = 2,
        Calendar = 3,
        Products = 4,
        ChildrensControll = 5,
    }
}