﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalBackend.Migrations
{
    public partial class base_user_avatars : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:permissions_enum", "admin,common,news,calendar,products,childrens_controll")
                .OldAnnotation("Npgsql:Enum:permissions_enum", "admin,common,news,calendar,products");

            migrationBuilder.AddColumn<string>(
                name: "AvatarLink",
                table: "users",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AvatarLink",
                table: "users");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:permissions_enum", "admin,common,news,calendar,products")
                .OldAnnotation("Npgsql:Enum:permissions_enum", "admin,common,news,calendar,products,childrens_controll");
        }
    }
}
