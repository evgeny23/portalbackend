﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalBackend.Models
{
    [Table("products")]
    public class Product
    {
        [Key]
        [MaxLength(32)]
        [Column(TypeName = "citext")]
        public string ProductId { get; set; }

        [MaxLength(256)]
        [Column(TypeName = "citext")]
        public string ProductName { get; set; }
    }
}
