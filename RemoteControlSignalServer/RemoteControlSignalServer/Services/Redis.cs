﻿using StackExchange.Redis;
using System;

namespace RemoteControlSignalServer
{
    internal static class Redis
    {
        private static ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(Config.RedisHost);

        public static void AddProduct(string serialKey, string data)
        {
            redis.GetDatabase(1).StringSet(serialKey, data);
            RedisKey key = new RedisKey(serialKey);
            redis.GetDatabase(1).KeyExpire(key, TimeSpan.FromSeconds(12));
        }

        public static string GetKeyData(string serialKey)
        {
            return redis.GetDatabase().StringGet(serialKey);
        }

        public static void GetTaskFromRedis()
        {
            foreach (var key in redis.GetDatabase(2).HashKeys("*"))
            {
                Console.WriteLine(key);
            }
        }
    }
}
