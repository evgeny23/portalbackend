﻿using PortalBackend.Models;
using PortalBackend.Responses;

namespace PortalBackend.Controllers.EducationController
{
    internal static class EducationDbMethods
    {
        internal static BaseResponse CreateNewEducationProgramm(EducationProgrammData data)
        {
            ApplicationContext db = new ApplicationContext();
            ProductEducation pe = new ProductEducation()
            {
                EduDesc = data.EduDesc,
                EduName = data.EduName,
                ImageLink = data.ImageLink,
                PageSource = data.PageSource,
                ProductId = data.ProductId,
            };
            db.Add(pe);
            db.SaveChanges();
            return StandartResponses.Ok("Учебная программа создана", pe);
        }
    }
}
