﻿using PortalBackend.Models;
using PortalBackend.Responses;
using PortalBackend.Services;
using System.Collections.Generic;
using System.Linq;

namespace PortalBackend.Controllers.Users.BaseUserController
{
    public class BaseUserDbMethods
    {
        internal static BaseResponse GetUsersWithPagination(int offset, int count)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                List<BaseUser> users = db.AllUsers.Skip(offset).Take(count).ToList();
                return StandartResponses.Ok("Список пользователей", users);
            }
        }

        internal static BaseResponse GetRelatedAccounts(int userId)
        {
            ApplicationContext db = new ApplicationContext();
            BaseUser user = db.AllUsers.Where(au => au.UserId == userId).FirstOrDefault();
            int baseUserId = userId;
            if (user.ParentUserId != 0)
                baseUserId = user.ParentUserId;
            List<BaseUser> allAccounts = db.AllUsers.Where(au => au.ParentUserId == baseUserId && au.UserId != userId).ToList();
            return StandartResponses.Ok("Список связанных аккаунтов", allAccounts);
        }

        internal static BaseResponse SetDefaultAccount(int userId)
        {
            ApplicationContext db = new ApplicationContext();
            int parentUserId = CommonDbMethods.GetMasterUserId(userId);
            if (parentUserId == userId)
                return StandartResponses.BadRequest("Мастер аккаунт не может быть аккаунтом по-умолчанию");
            BaseUser user = db.AllUsers.Where(au => au.UserId == userId && au.ParentUserId == parentUserId).FirstOrDefault();
            if (user == null)
                return StandartResponses.Forbidden("Ошибка доступа");
            List<BaseUser> users = db.AllUsers.Where(au => au.ParentUserId == parentUserId).ToList();
            users.ForEach(u => u.DefaultAccount = false);
            user.DefaultAccount = true;
            db.AllUsers.Update(user);
            db.SaveChanges();
            return StandartResponses.Ok("Аккаунт установлен аккаунтом по-умолчанию", user);
        }

        internal static BaseResponse SetAccountNotDefault(int userId)
        {
            ApplicationContext db = new ApplicationContext();
            int parentUserId = CommonDbMethods.GetMasterUserId(userId);
            if (parentUserId == userId)
                return StandartResponses.BadRequest("Указанный id принадлежит мастер-аккаунту");
            BaseUser user = db.AllUsers.Where(au => au.UserId == userId && au.ParentUserId == parentUserId).FirstOrDefault();
            if (user == null)
                return StandartResponses.Forbidden("Ошибка доступа");
            user.DefaultAccount = false;
            db.AllUsers.Update(user);
            db.SaveChanges();
            return StandartResponses.Ok("Аккаунт сделан не дефолтным", user);
        }

        internal static BaseResponse AddAvatarToUser(int userId, string link)
        {
            ApplicationContext db = new ApplicationContext();
            BaseUser user = db.AllUsers.Where(au => au.UserId == userId).FirstOrDefault();
            user.AvatarLink = link;
            db.AllUsers.Update(user);
            db.SaveChanges();
            return StandartResponses.Ok("Аватар успешно установлен", link);
        }

        internal static BaseResponse DeleteAvatar(int userId)
        {
            ApplicationContext db = new ApplicationContext();
            BaseUser user = db.AllUsers.Where(au => au.UserId == userId).FirstOrDefault();
            user.AvatarLink = null;
            db.AllUsers.Update(user);
            db.SaveChanges();
            return StandartResponses.Ok("Аватар успешно удалён");
        }
    }
}
