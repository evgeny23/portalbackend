﻿using System.Text.RegularExpressions;

namespace PortalBackend.Security
{
    public static class Validators
    {
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsValidPhoneNumber(string number)
        {
            return Regex.Match(number, @"^(\+[0-9]{7,14})$").Success;
        }

        public static bool IsValidPassword(string password)
        {
            if (password.Length < 6)
                return false;
            return true;
        }

        public static bool IsValidInn(string inn)
        {
            return Regex.Match(inn, @"^([0-9]{12})$").Success;
        }
    }
}
