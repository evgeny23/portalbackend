﻿namespace PortalBackend.Controllers.ProductGamesController
{
    public class GameData
    {
        /// <summary>
        /// Ключ продукта, которому принадлежит игра
        /// </summary>
        public string productKey { get; set; }

        /// <summary>
        /// Идентификатор игры (должен соответствовать значению из enum в самой игре)
        /// </summary>
        public int gameId { get; set; }

        /// <summary>
        /// Название игры
        /// </summary>
        public string name { get; set; }
    }
}
