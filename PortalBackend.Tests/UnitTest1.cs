using System;
using Xunit;
using PortalBackend.Controllers.OAuth2Controller;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace PortalBackendApp.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var data = new AuthenticationData
            {
                email = "ek993@mail.ru",
                password = "12345678",
            };

            // Arrange
            OAuth2Controller controller = new OAuth2Controller();

            // Act
            ViewResult result = controller.Authentication(data) as ViewResult;

            Assert.NotNull(result);
        }
    }
}
