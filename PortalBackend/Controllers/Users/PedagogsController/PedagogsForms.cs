﻿namespace PortalBackend.Controllers.Users.PedagogsController
{
    public class PedagogData
    {
        public string FIO { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string Subjects { get; set; }
    }
}
