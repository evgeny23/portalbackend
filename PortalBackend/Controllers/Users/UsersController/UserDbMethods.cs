﻿using PortalBackend.Controllers.OAuth2Controller;
using PortalBackend.Models;
using PortalBackend.Responses;
using PortalBackend.Security;
using PortalBackend.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Z.EntityFramework.Plus;

namespace PortalBackend.Controllers.UsersController
{
    public static class UserDbMethods
    {
        internal static BaseResponse CreateNewUser(RegisterData data)
        {
            var Response = ValidateUserData(data);
            if (Response != null)
                return Response;
            using (ApplicationContext db = new ApplicationContext())
            {
                if (db.Users.Where(
                    u => u.Email == data.email || (data.phoneNumber != null && u.PhoneNumber == data.phoneNumber)
                ).FirstOrDefault() != null )
                {
                    return StandartResponses.BadRequest("Пользователь уже существует");
                }

                User newUser = CreateNewUser(new User(), data);
                db.Users.AddRange(newUser);
                db.SaveChanges();
                DefaultRoles.SetUserRole(newUser.UserId, DefaultRolesEnum.Registered);

                (string cid, string key) = ActivationCodes.Create(SecurityConfig.ACTIVATION_EMAIL_LIFETIME
                    , newUser.UserId
                    , ActivationType.Email
                    , SecurityConfig.ACTIVATION_EMAIL_RESEND_TIMEOUT);
                string link = String.Format("http://localhost:3000/confirmEmail1?cid={0}&key={1}", cid, key);
                Mail.SendActivationEmail(newUser.Email, link);
                return StandartResponses.Ok("Пользователь успешно зарегистрирован", newUser);
            }
        }

        internal static BaseResponse UpdateUser(int userId, RegisterData data)
        {
            var Response = ValidateUserData(data);
            if (Response != null)
                return Response;
            ApplicationContext db = new ApplicationContext();
            userId = CommonDbMethods.GetMasterUserId(userId);
            User user = db.Users.Where(u => u.UserId == userId).FirstOrDefault();
            Dictionary<string, string> result = PasswordHashing.GetPasswordHashAndSalt(data.password, user.Salt);
            if (result["hash"] != user.PasswordHash)
            {
                return StandartResponses.BadRequest("Неверный пароль");
            }
            user.Email = data.email;
            user.PhoneNumber = data.phoneNumber;
            db.Update(user);
            db.SaveChanges();
            return StandartResponses.Ok("Данные пользователя обновлены", user);
        }

        internal static BaseResponse UpdateUserPassword(int userId, string newPassword, string oldPassword)
        {
            ApplicationContext db = new ApplicationContext();
            if (Validators.IsValidPassword(newPassword))
                return StandartResponses.BadRequest("Недостаточно сложный пароль");
            userId = CommonDbMethods.GetMasterUserId(userId);
            User user = db.Users.Where(u => u.UserId == userId).FirstOrDefault();
            Dictionary<string, string> result = PasswordHashing.GetPasswordHashAndSalt(oldPassword, user.Salt);
            if (result["hash"] != user.PasswordHash)
                return StandartResponses.BadRequest("Неверный пароль");
            result = PasswordHashing.GetPasswordHashAndSalt(newPassword);
            user.PasswordHash = result["hash"];
            user.Salt = result["salt"];
            db.Users.Update(user);
            db.SaveChanges();
            return StandartResponses.Ok("Пароль успешно обновлён");
        }

        internal static BaseResponse DeleteUser(int userId)
        {
            User user = CommonDbMethods.GetUserById(userId) as User;
            if (user == null)
                return StandartResponses.BadRequest("Ошибка при получении пользователя");
            ApplicationContext db = new ApplicationContext();
            db.Users.Remove(user);
            db.SaveChanges();
            return StandartResponses.Ok("Пользователь удалён");
        }

        internal static BaseResponse ActivateEmail(int userId) 
        {
            User user = CommonDbMethods.GetUserById(userId) as User;
            if (user == null)
                return StandartResponses.BadRequest("Ошибка при получении пользователя");
            ApplicationContext db = new ApplicationContext();
            user.EmailValidate = true;
            db.Users.Update(user);
            db.SaveChanges();
            return StandartResponses.Ok("Email успешно подтвержден");
        }

        internal static BaseResponse RestorePasswordEmail(RestorePasswordEmailData data)
        {
            if (!Validators.IsValidEmail(data.email))
                return StandartResponses.BadRequest("Введён невалидный email");
            ApplicationContext db = new ApplicationContext();
            User user = db.Users.Where(u => u.Email == data.email).FirstOrDefault();
            if (user == null)
                StandartResponses.BadRequest("Ошибка выполнения запроса");
            (string cid, string key) = ActivationCodes.Create(SecurityConfig.RESTORE_PASSWORD_LIFETIME
                , user.UserId
                , ActivationType.PasswordReset
                , SecurityConfig.RESTORE_PASSWORD_RESEND_TIMEOUT);
            if (cid == null)
                return StandartResponses.BadRequest("Таймаут на попытки восстановления пароля");
            string link = String.Format("http://localhost:3000/restore_password1?cid={0}&key={1}", cid, key);
            Mail.SendRestoreEmail(user.Email, link);
            return StandartResponses.Ok("Выслано письмо с инструкциями: " + link);
        }

        internal static BaseResponse RestoreSetNetPassword(NewPasswordData data, string cid, string key)
        {
            int userId = ActivationCodes.Check(cid, key);
            if (userId == -1)
                return StandartResponses.BadRequest("Ссылка недействительна");
            if (!Validators.IsValidPassword(data.password))
                return StandartResponses.BadRequest("Пароль недостаточно сложный");
            /*            user.PasswordHash = result["hash"];
                        user.Salt = result["salt"];*/
            using (ApplicationContext db = new ApplicationContext())
            {
                Dictionary<string, string> result = PasswordHashing.GetPasswordHashAndSalt(data.password);
                User user = db.Users.Where(u => u.UserId == userId).FirstOrDefault();
                user.PasswordHash = result["hash"];
                user.Salt = result["salt"];
                db.Users.Update(user);
                db.SaveChanges();
                return StandartResponses.Ok("Новый пароль успешно установлен");
            }
        }

        internal static BaseResponse GetUsersWithPagination(int offset, int count)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                List<BaseUser> users = db.AllUsers.Skip(offset).Take(count).ToList();
                return StandartResponses.Ok("Список пользователей", users);
            }
        }

        private static BaseResponse ValidateUserData(RegisterData data)
        {
            if (!Validators.IsValidEmail(data.email))
            {
                return StandartResponses.BadRequest("Некорректный email");
            }
            if (data.phoneNumber != null && !Validators.IsValidPhoneNumber(data.phoneNumber))
            {
                return StandartResponses.BadRequest("Некорректный телефонный номер");
            }
            if (data.password.Length < 8)
            {
                return StandartResponses.BadRequest("Слишком короткий пароль");
            }
            return null;
        }

        private static User CreateNewUser(User user, RegisterData data)
        {
            user.Email = data.email;
            if (data.phoneNumber != null)
                user.PhoneNumber = data.phoneNumber;
            user.RegistrationDate = DateTime.Now;
            Dictionary<string, string> result = PasswordHashing.GetPasswordHashAndSalt(data.password);
            user.PasswordHash = result["hash"];
            user.Salt = result["salt"];
            return user;
        }
    }
}
