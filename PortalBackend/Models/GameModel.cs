﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PortalBackend.Services;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalBackend.Models
{
    [Table("product_games")]
    public class Game
    {
        public int GameId { get; set; }

        public string ProductId { get; set; }
        [JsonIgnore]
        public Product Product { get; set; }

        [Column(TypeName = "citext")]
        [MaxLength(1024)]
        [MinLength(2)]
        public string GameName { get; set; }

        [NotMapped]
        public bool IsRun { get; set; }

        public bool CheckIsRun(string productSerialHash)
        {
            string data = Redis.GetData(productSerialHash, (int)RedisDatabases.OnlineProducts);
            if (data == null)
                return false;
            try
            {
                var json = JsonConvert.DeserializeObject<JToken>(data);
                var value = json["games"][productSerialHash];
                if (value == null)
                    return false;
                return Boolean.Parse(json["games"][productSerialHash][this.GameId.ToString()].ToString());
            }
            catch
            {
                return false;
            }
        }
    }
}
