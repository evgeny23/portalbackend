﻿using PortalBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBackend.Services
{
    public static class CommonDbMethods
    {
        public static int GetMasterUserId(int userId)
        {
            ApplicationContext db = new ApplicationContext();
            BaseUser user = db.AllUsers.Where(au => au.UserId == userId).FirstOrDefault();
            if (user == null)
                return -1;
            if (user.ParentUserId == 0)
                return userId;
            return user.ParentUserId;
        }

        public static BaseUser GetUserById(int id)
        {
            ApplicationContext db = new ApplicationContext();
            BaseUser user = db.AllUsers.Where(u => u.UserId == id).FirstOrDefault();
            return user;
        }
    }
}
