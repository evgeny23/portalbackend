using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Responses;
using PortalBackend.Security;
using System;
using System.Collections.Generic;

namespace PortalBackend.Controllers.ProductGamesController
{
    [Route("api/product_games")]
    [ApiController]
    public class ProductGamesController : ControllerBase
    {
        #region Админские роуты
        /// <summary>
        /// Добавление новой игры
        /// [CheckPermissions(PermissionsEnum.Admin)]
        /// </summary>
        /// <param name="data">{{GameData}}</param>
        /// <response code="200">Игра добавлена</response>
        /// <response code="400">Ошибка при добавлении игры</response>
        [HttpPost("add_new_game")]
        [CheckPermissions(PermissionsEnum.Admin)]
        public IActionResult AddNewProductGame([FromBody] GameData data)
        {
            return DbMethods.AddNewGameToProduct(data).Resp();
        }

        /// <summary>
        /// Добавление нового тега к игре
        /// [CheckPermissions(PermissionsEnum.Admin)]
        /// </summary>
        /// <param name="productKey">Ключ продукта</param>
        /// <param name="gameId">Идентификатор игры</param>
        /// <param name="tag">Тег игры</param>
        /// <response code="200">Тег успешно добавлен</response>
        /// <response code="400">Тег уже существует</response>
        [HttpPost("add_new_tag/{productKey}/{gameId}/{tag}")]
        [CheckPermissions(PermissionsEnum.Admin)]
        public IActionResult AddNewTagToGame(string productKey, int gameId, string tag)
        {
            return DbMethods.AddTagToGame(productKey, gameId, tag).Resp();
        }
        #endregion

        #region Пользовательские роуты
        /// <summary>
        /// Получения игры по id
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <param name="productKey">Ключ продукта</param>
        /// <param name="serialHash">Хеш ключа продукта</param>
        /// <param name="gameId">Идентификатор игры</param>
        /// <response code="200">Игра получениа</response>
        /// <response code="400">Игра с таким id не существует</response>
        [HttpGet("get_game_by_id/{userId}/{productKey}/{serialHash}/{gameId}")]
        [CheckPermissions(PermissionsEnum.Common, "userId")]
        public IActionResult GetProductGameById(int userId, string productKey, string serialHash, int gameId)
        {
            return DbMethods.GetGameById(productKey, serialHash, gameId, userId).Resp();
        }

        /// <summary>
        /// Получение списка игр продукта с пагинацией
        /// [CheckPermissions(PermissionsEnum.Common, "userId")]
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <param name="productKey"></param>
        /// <param name="serialHash"></param>
        /// <param name="offset">Смещение</param>
        /// <param name="count">Количество игр</param>
        /// <response code="200">Список игр продукта предоставлен</response>
        /// <response code="400">Ошибка в offset или count</response>
        [HttpGet("get_games_list/{userId}/{productKey}/{serialHash}/{offset}/{count}")]
        [CheckPermissions(PermissionsEnum.Common, "userId")]
        public IActionResult GetGames(int userId, string productKey, string serialHash, int offset, int count)
        {
            if (offset < 0 || count < 1 || count > Config.maxPaginationCountLimit)
            {
                return StandartResponses.BadRequest("Неверные offset/count").Resp();
            }
            return DbMethods.GetGamesWithPagination(productKey, serialHash, userId, offset, count).Resp();
        }

        /// <summary>
        /// Получение списка тегов игры с пагинацией
        /// [CheckPermissions(PermissionsEnum.Common)]
        /// </summary>
        /// <param name="productKey">Ключ продукта</param>
        /// <param name="gameId">Идентификатор игры</param>
        /// <param name="offset">смещение</param>
        /// <param name="count">количество</param>
        /// <response code="200">Список тегов получен</response>
        /// <response code="400">Игры с таким id не существует</response>
        [HttpGet("get_game_tags/{productKey}/{gameId}/{offset}/{count}")]
        [CheckPermissions(PermissionsEnum.Common)]
        public IActionResult GetGameTagsWithPaginatoin(string productKey, int gameId, int offset, int count)
        {
            return DbMethods.GetTagsWithPagination(productKey, gameId, offset, count).Resp();
        }

        /// <summary>
        /// Получение списка игра по тегу/тегам
        /// [CheckPermissions(PermissionsEnum.Common)]
        /// </summary>
        /// <param name="tags"></param>
        /// <response code="200">Список тегов получен</response>
        /// <response code="400">Игры с таким id не существует</response>
        [HttpGet("get_games_by_tag_list/{tags}")]
        [CheckPermissions(PermissionsEnum.Common)]
        public IActionResult GetGamesListByTags(string tags)
        {
            List<string> t = new List<string>();
            t.AddRange(tags.Split(','));
            return DbMethods.GetGamesByTags(t).Resp();
        }
        #endregion
    }
}
