﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PortalBackend.Services;

namespace PortalBackend.Models
{
    [Table("users_products")]
    public class UserProduct
    {
        public string ProductId { get; set; }
        [JsonIgnore]
        public Product Product { get; set; }

        [Key]
        public string SerialHash { get; set; }

        public int UserId { get; set; }
        [JsonIgnore]
        public BaseUser User { get; set; }

        [NotMapped]
        public bool IsOnline
        {
            get 
            {
                return Redis.GetData(this.SerialHash, (int)RedisDatabases.OnlineProducts) != null;
            }
        }

        [NotMapped]
        public bool IsRun
        {
            get 
            {
                string data = Redis.GetData(this.SerialHash, (int)RedisDatabases.OnlineProducts);
                if (data == null)
                    return false;
                try
                {
                    var json = JsonConvert.DeserializeObject<JToken>(data);
                    return json["games"][this.SerialHash] != null;
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}
