﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalBackend.Migrations
{
    public partial class remove_subjects : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Subjects",
                table: "users");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Subjects",
                table: "users",
                type: "text",
                nullable: true);
        }
    }
}
