﻿using RabbitMQ.Client;
using Newtonsoft.Json;
using System.Diagnostics;
using PortalBackend.Services;

namespace PortalBackend.Controllers.UserProductController
{
    public static class RabbitSend
    {

        public static bool AddCommand(object command)
        {
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };
            string json = JsonConvert.SerializeObject(command, settings);
            Debug.WriteLine(json);
            byte[] messageBodyBytes = System.Text.Encoding.UTF8.GetBytes(json);
            Debug.WriteLine(messageBodyBytes);
            IModel channel = Rabbit.GetChannel();
            if (channel == null)
                return false;
            channel.BasicPublish(exchange: "pc", 
                                 routingKey: "command",
                                 basicProperties:null, 
                                 body: messageBodyBytes);
            return true;
        }
    }
}
