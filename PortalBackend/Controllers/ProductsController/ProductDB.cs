﻿using PortalBackend.Models;
using PortalBackend.Responses;
using System.Collections.Generic;
using System.Linq;
using Z.EntityFramework.Plus;

namespace PortalBackend.Controllers.ProductsController
{
    public static class ProductDB
    {
        internal static BaseResponse AddProduct(ProductData data)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                if (db.Products.Where(p => p.ProductId == data.productId).FirstOrDefault() != null)
                {
                    return StandartResponses.BadRequest("Такой продукт уже существует");
                }
                Product product = new Product { ProductId = data.productId, ProductName = data.productName };
                db.Products.Add(product);
                db.SaveChanges();
                return StandartResponses.Ok("Продукт добавлен", product);
            }
        }

        internal static BaseResponse GetProduct(string key)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                Product product = db.Products.Where(p => p.ProductId == key).FirstOrDefault();
                if (product == null)
                    return StandartResponses.BadRequest("Такого продукта не существует");
                return StandartResponses.Ok("Продукт с заданным id", product);
            }
        }

        internal static BaseResponse GetProducts(int offset, int count)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                List<Product> products = db.Products.OrderByDescending(x => x.ProductId).Skip(offset).Take(count).ToList();
                return StandartResponses.Ok("Список продуктов", products);
            }
        }

        internal static BaseResponse UpdateProduct(ProductData data)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var result = db.Products.Where(p => p.ProductId == data.productId).Update(
                    p => new Product { 
                        ProductId = data.productId, 
                        ProductName = data.productName 
                    }) > 0;
                db.SaveChanges();
                if (result)
                    return StandartResponses.Ok("Продукт успешно обновлён");
                return StandartResponses.BadRequest("Ошибка при обновлении продукта");
            }
        }

        internal static BaseResponse DeleteProduct(string key)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var result = db.Products.Where(p => p.ProductId == key).Delete() > 0;
                db.SaveChanges();
                if (result)
                    return StandartResponses.Ok("Продукт успешно удалён");
                return StandartResponses.BadRequest("Не удалось удалить продукт");
            }
        }
    }
}
