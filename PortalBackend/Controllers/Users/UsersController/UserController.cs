﻿using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Models;
using PortalBackend.Responses;
using PortalBackend.Security;
using PortalBackend.Services;
using System;

namespace PortalBackend.Controllers.UsersController
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region создание и редактирование
        /// <summary>
        /// Регистрация нового пользователя
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса</response>
        [HttpPost]
        public IActionResult Post([FromBody] RegisterData data)
        {
            return UserDbMethods.CreateNewUser(data).Resp();
        }

        /// <summary>
        /// Обновление существующего пользователя. Доступно только самому пользователю и админам.
        /// В id надо передавать id текущего дочернего аккаунта, не мастера
        /// [CheckPermissions(PermissionsEnum.Common, "id", true)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса.</response>
        /// <response code="401">Не авторизован</response> 
        /// /// <response code="403">Доступ к ресурсу запрещён</response>
        [HttpPut("update_user/{id}")]
        [CheckPermissions(PermissionsEnum.Common, "id", true)]
        public IActionResult Put(int id, [FromBody] RegisterData data)
        {
            return UserDbMethods.UpdateUser(id, data).Resp();
        }

        /// <summary>
        /// Обновление пароля существующего пользователя. Доступно только самому пользователю и админам
        /// [CheckPermissions(PermissionsEnum.Common, "id", true)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса.</response>
        /// <response code="401">Не авторизован</response> 
        /// /// <response code="403">Доступ к ресурсу запрещён</response>
        [HttpPut("update_password/{id}")]
        [CheckPermissions(PermissionsEnum.Common, "id", true)]
        public IActionResult UpdateUserPassword(int id, [FromBody] UpdatePasswordData data)
        {
            return UserDbMethods.UpdateUserPassword(id, data.newPassword, data.oldPassword).Resp();
        }
        #endregion

        #region активация/восстановление
        /// <summary>
        /// Запрос на активацию аккаунта по email
        /// [CheckPermissions(PermissionsEnum.Common, "userId", true)]
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns></returns>
        [HttpGet("/activation/email/send/{userId}")]
        [CheckPermissions(PermissionsEnum.Common, "userId", true)]
        public IActionResult EmailActivation(int userId)
        {
            userId = CommonDbMethods.GetMasterUserId(userId);
            User user = CommonDbMethods.GetUserById(userId) as User;
            if (user.EmailValidate)
                return StandartResponses.BadRequest("Email уже подтвержден").Resp();

            (string cid, string key) = ActivationCodes.Create(SecurityConfig.ACTIVATION_EMAIL_LIFETIME
                , userId
                , ActivationType.Email
                , SecurityConfig.ACTIVATION_EMAIL_RESEND_TIMEOUT);

            if (cid == null)
                return StandartResponses.BadRequest("Слишком частые запросы на отправку письма актвиации").Resp();

            string link = String.Format("http://localhost:3000/confirmEmail1?cid={0}&key={1}", cid, key);
            Mail.SendActivationEmail(user.Email, link);
            return StandartResponses.Ok("/activation/email/check/" + cid + "/" + key).Resp();
        }

        /// <summary>
        /// Восстановление пароля по email
        /// </summary>
        /// <param name="data">RestorePasswordEmail</param>
        /// <returns></returns>
        [HttpPost("/restore_password/email/send")]
        public IActionResult RestorePasswordEmail([FromBody] RestorePasswordEmailData data)
        {
            return UserDbMethods.RestorePasswordEmail(data).Resp();
        }

        /// <summary>
        /// Роут для установки нового пароля
        /// </summary>
        /// <param name="data">password: %NewPassword%</param>
        /// <param name="cid">Идентификатор кода</param>
        /// <param name="key">Секретный код</param>
        /// <returns></returns>
        [HttpPut("/restore_password/email/check/{cid}/{key}")]
        public IActionResult SetNewPassword([FromBody] NewPasswordData data, string cid, string key)
        {
            return UserDbMethods.RestoreSetNetPassword(data, cid, key).Resp();
        }

        /// <summary>
        /// Проверка кода активации
        /// </summary>
        /// <param name="cid">Code id</param>
        /// <param name="key">Код активации</param>
        /// <returns></returns>
        [HttpGet("/activation/email/check/{cid}/{key}")]
        public IActionResult EmailActivationCheckCode(string cid, string key)
        {
            int userId = ActivationCodes.Check(cid, key);
            if (userId != -1)
            {
                return UserDbMethods.ActivateEmail(userId).Resp();
            }
            return StandartResponses.BadRequest("Неверный код активации").Resp();
        }
        #endregion

        #region удаление/бан/деактивация
        /// <summary>
        /// Удаление существующего пользователя. Доступно только админам
        /// [CheckPermissions(PermissionsEnum.Admin)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса.</response>
        /// <response code="401">Не авторизован</response> 
        /// /// <response code="403">Доступ к ресурсу запрещён</response>
        [HttpDelete("{id}")]
        [CheckPermissions(PermissionsEnum.Admin)]
        public IActionResult Delete(int id)
        {
            return UserDbMethods.DeleteUser(id).Resp();
        }
        #endregion
    }
}
