﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBackend.Models
{
    [Table("edu_courses")]
    public class EduCourse
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EduId { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(512)]
        public string Name { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(8096)]
        public string Desc { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(256)]
        public string Image { get; set; }

        [Required]
        [Column(TypeName = "citext")]
        public string PageSource { get; set; }
    }
}
