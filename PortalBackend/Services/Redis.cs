﻿using StackExchange.Redis;
using System;

namespace PortalBackend.Services
{
    public static class Redis
    {
        private static ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(Config.RedisHost);

        public static void AddData(string key, string value, int database, int lifetime = -1)
        {
            redis.GetDatabase(database).StringSet(key, value);
            RedisKey redisKey = new RedisKey(key);
            redis.GetDatabase(database).KeyExpire(redisKey, lifetime != -1 ? TimeSpan.FromSeconds(lifetime) : null);
        }

        public static string GetData(string key, int database)
        {
            return redis.GetDatabase(database).StringGet(key);
        }

        public static bool RemoveData(string key, int database)
        {
            RedisKey redisKey = new RedisKey(key);
            return redis.GetDatabase(database).KeyDelete(redisKey);
        }
    }
}