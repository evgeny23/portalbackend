﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBackend.Controllers.GartensController
{
    public class RegisterGartenData
    {
        public string INN { get; set; }

        public string Name { get; set; }
    }
}
