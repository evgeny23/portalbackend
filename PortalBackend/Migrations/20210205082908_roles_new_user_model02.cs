﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalBackend.Migrations
{
    public partial class roles_new_user_model02 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "role_permissions",
                columns: new[] { "Permission", "RoleId" },
                values: new object[] { 1, 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "role_permissions",
                keyColumns: new[] { "Permission", "RoleId" },
                keyValues: new object[] { 1, 2 });
        }
    }
}
