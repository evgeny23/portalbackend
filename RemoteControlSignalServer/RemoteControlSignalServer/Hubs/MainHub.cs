﻿using Microsoft.AspNetCore.SignalR;
using ProductsProtocol;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Text;
using System;
using System.ServiceModel;

namespace RemoteControlSignalServer.Hubs
{
    public class MainHub: Hub
    {
        private readonly ILogger _logger;
        private static readonly System.Timers.Timer _timer = new System.Timers.Timer();

        public MainHub(ILogger<MainHub> logger)
        {
            _logger = logger;
        }

        public void ConnectClient(string payload)
        {
            _logger.LogInformation(payload);
            ConnectData connectData = JsonConvert.DeserializeObject<ConnectData>(payload);
            foreach (KeyValuePair<string, string> entry in connectData.keys)
            {
                _logger.LogInformation("=================");
                _logger.LogInformation(entry.Value);
                if (entry.Value.Length == 0)
                    continue;
                string serialKey;
                if (ConnectedClients.GetMd5keyByB64Key(entry.Value) != null)
                {
                    serialKey = ConnectedClients.GetMd5keyByB64Key(entry.Value);
                }
                else
                {
                    serialKey = getDecryptKey(entry.Key, entry.Value);
                    _logger.LogInformation(entry.Key + "," + entry.Value);
                    _logger.LogInformation(serialKey);
                    _logger.LogInformation("=================");
                    if (serialKey == null)
                    {
                        continue;
                    }
                    serialKey = CreateMD5(serialKey);
                }
                // Переименуем ключ в games из b64 формата в md5, понятный бэкенду
                if (connectData.games.ContainsKey(entry.Value))
                {
                    var data = connectData.games[entry.Value];
                    connectData.games.Remove(entry.Value);
                    connectData.games.Add(serialKey, data);
                }
                Redis.AddProduct(serialKey, JsonConvert.SerializeObject(connectData));
                ConnectedClients.AddClient(entry.Value, serialKey, Clients.Caller);
                //_logger.LogInformation(serialKey);
                //_logger.LogInformation(ConnectedClients.GetClientBySerialKey(serialKey).ToString());
            }
        }

        public void OnDisconnected()
        {
            _logger.LogInformation(Context.ConnectionId);
        }

        public static string CreateMD5(string input)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        private static WCFServiceClient CreateClient()
        {
            var binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            var uri = "http://130.193.35.44:9011/UserDeterminerService";
            var endpoint = new EndpointAddress(new Uri(uri));
            return new WCFServiceClient(binding, endpoint);
        }

        private string getDecryptKey(string product, string serialKey)
        {
            try
            {
                var client = CreateClient();
                return client.DecryptedKey(product, serialKey);
            } catch
            {
                return null;
            }
        }
    }
}
