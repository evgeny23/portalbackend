﻿using Microsoft.AspNetCore.Mvc;
using PortalBackend.Filters.CheckPermissions;
using PortalBackend.Responses;
using PortalBackend.Security;
using PortalBackend.Services;
using System;

namespace PortalBackend.Controllers.GartensController
{
    [Route("api/gartens")]
    [ApiController]
    public class GartensController : ControllerBase
    {
        /// <summary>
        /// Регистрация нового детского сада
        /// [CheckPermissions(PermissionsEnum.Common, "", true)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса</response>
        [HttpPost]
        [CheckPermissions(PermissionsEnum.Common, "", true)]
        public IActionResult Post([FromBody] RegisterGartenData data)
        {
            int userId = CommonDbMethods.GetMasterUserId(Convert.ToInt32(JwtTokens.GetValueFromPayload(Request.Headers, "userId")));
            return GartensDbMethods.CreateNewGarten(data, userId).Resp();
        }

        /// <summary>
        /// Обновление информации детского сада
        /// [CheckPermissions(PermissionsEnum.Common, "", true)]
        /// </summary>
        /// <response code="200">Успешно</response>
        /// <response code="400">Ошибка выполнения запроса</response>
        [HttpPut]
        [CheckPermissions(PermissionsEnum.Common, "id", true)]
        public IActionResult Put(int id, [FromBody] RegisterGartenData data)
        {
            return GartensDbMethods.UpdateGartenData(id, data).Resp();
        }

        /// <summary>
        /// Добавление педагога к дет.саду
        /// [CheckPermissions(PermissionsEnum.Common, "gartenId", true)]
        /// </summary>
        /// <param name="gartenId">ИД детского сада</param>
        /// <param name="pedagogId">ИД педагога</param>
        /// <returns></returns>
        [HttpPost("add_pedagog/{gartenId}/{pedagogId}")]
        [CheckPermissions(PermissionsEnum.Common, "gartenId", true)]
        public IActionResult AddPedagog(int gartenId, int pedagogId)
        {
            return GartensDbMethods.AddPedagog(gartenId, pedagogId).Resp();
        }

        /// <summary>
        /// Получение списка педагогов садика с пагинацией
        /// </summary>
        /// <param name="gartenId">ИД детского сада</param>
        /// <param name="offset">Смещение</param>
        /// <param name="count">Количество</param>
        /// <returns></returns>
        [HttpGet("get_pedagogs/list/{gartenId}/{offset}/{count}")]
        [CheckPermissions(PermissionsEnum.Common, "gartenId", true)]
        public IActionResult GetGartenPedagogs(int gartenId, int offset, int count)
        {
            if (offset < 0 || count < 1 || count > Config.maxPaginationCountLimit)
            {
                return StandartResponses.BadRequest("Неверные offset/count").Resp();
            }
            return GartensDbMethods.GetGartenPedagogsWithPagination(gartenId, count, offset).Resp();
        }

        /// <summary>
        /// Открепление педагога от детского сада
        /// [CheckPermissions(PermissionsEnum.Common, "gartenId", true)]
        /// </summary>
        /// <param name="gartenId">ИД детского сада</param>
        /// <param name="pedagogId">ИД педагога</param>
        /// <returns></returns>
        [HttpDelete("add_pedagog/{gartenId}/{pedagogId}")]
        [CheckPermissions(PermissionsEnum.Common, "gartenId", true)]
        public IActionResult DeletePedagogFromGarten(int gartenId, int pedagogId)
        {
            return GartensDbMethods.RemovePedagogFromGarten(gartenId, pedagogId).Resp();
        }
    }
}
